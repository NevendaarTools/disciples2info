﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TreeView;

namespace Disciples2Info
{
    public class BuildingsProvider : ViewProviderBase, InfoProvider
    {
        private readonly Font nameFont = new Font("comic sans", 15, FontStyle.Bold);
        private readonly Font descFont = new Font("comic sans", 15);
        private const int offsetX = 180;
        private const int rowH = 36;
        Dictionary<string, List<string>> upgrBinding = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);

        public string GetInfoType()
        {
            return "BB";
        }

        public BuildingsProvider(GameModel model_)
        {
            model = model_;
            foreach (Gunit unit in model.GetAllT<Gunit>())
            {
                if (unit.unit_cat.value.text == "L_LEADER")
                    continue;
                string id = "";
                if (unit.upgrade_b.value != null)
                {
                    id = unit.upgrade_b.value.build_id;
                }
                if (unit.enroll_b.value != null)
                {
                    id = unit.enroll_b.value.build_id;
                }
                if (id != "")
                {
                    if (upgrBinding.ContainsKey(id))
                        upgrBinding[id].Add(unit.unit_id);
                    else
                        upgrBinding.Add(id, new List<string>() { unit.unit_id });
                }
            }
            var raceMapping = new Dictionary<string, string>
            {
                { "HU", "g000LR0003" },
                { "UN", "g000LR0012" },
                { "HE", "g000LR0009" },
                { "DW", "g000LR0006" },
                { "EL", "g000LR0018" }
            };

            List<GbuiList> gbuiList = model.GetAllT<GbuiList>();
            List<Gbuild> allBuildings = model.GetAllT<Gbuild>();
            List<Lbuild> categories = model.GetAllT<Lbuild>();

            ComboBoxFilter raceFilter = new ComboBoxFilter();
            raceFilter.Init(model, new Size(140, 20), "<Race>", (entry, str) =>
            {
                var lordId = raceMapping.FirstOrDefault(m => m.Key == str).Value;
                var buildIds = gbuiList
                    .Where(g => g.lord_id.key == lordId)
                    .Select(g => g.build_id.key)
                    .ToList();

                return buildIds.Any() && buildIds.Contains((entry.item as Gbuild).build_id);
            });

            foreach (var race in raceMapping.Keys)
            {
                raceFilter.AddVariant(new FilterValue(race, race));
            }

            ComboBoxFilter typeFilter = new ComboBoxFilter();
            typeFilter.Init(model, new Size(140, 20), "<Category>", (entry, str) =>
            {
                Lbuild categoryObj = (entry.item as Gbuild).category.value;
                return categoryObj.id == (int)str;
            });

            foreach (var category in categories)
            {
                typeFilter.AddVariant(new FilterValue(category.id, category.text));
            }

            foreach (Gbuild build in allBuildings)
            {
                string context = build.name_txt.value.text;
                allItems.Add(new InfoEntry() { context = context, id = build.build_id, item = build });
            }

            allItems.Sort((x, y) => string.Compare(x.context, y.context));

            int minLevel = allBuildings.Min(b => b.level);
            int maxLevel = allBuildings.Max(b => b.level);

            ComboBoxFilter levelFilter = new ComboBoxFilter();
            levelFilter.Init(model, new Size(80, 20), "<Level>", (entry, str) => ((Gbuild)entry.item).level == (int)str);

            for (int i = minLevel; i <= maxLevel; i++)
            {
                levelFilter.AddVariant(new FilterValue(i, "Level " + i.ToString()));
            }

            ComboBoxFilter branchFilter = new ComboBoxFilter();
            branchFilter.Init(model, new Size(80, 20), "<Branch>", (entry, str) => ((Gbuild)entry.item).branch.key == (int)str);
            var branches = model.GetAllT<LunitB>();
            branchFilter.AddVariant(new FilterValue(-1, "None"));
            foreach (var branch in branches)
            {
                if (branch.id > 4)
                    break;
                branchFilter.AddVariant(new FilterValue(branch.id, branch.text));
            }

            filters.Add(levelFilter);
            filters.Add(typeFilter);
            filters.Add(branchFilter);
            filters.Add(raceFilter);
        }

        public ViewItem GetView(IViewIntent intent)
        {
            ViewItem result = new ViewItem();
            ViewIntentBase baseIntent = intent as ViewIntentBase;
            currentId = baseIntent.id;
            Gbuild build = model.GetObjectT<Gbuild>(baseIntent.id);
            Image image = StyleService.GetImage("back");

            if (image == null)
            {
                image = new Bitmap(1000, 410);

                using (Graphics gfx = Graphics.FromImage(image))
                using (SolidBrush brush = new SolidBrush(StyleService.GetColor("back")))
                {
                    gfx.FillRectangle(brush, 0, 0, image.Width, image.Height);
                }
            }

            image = GameResourceModel.ResizeImage(image, new Size(1000, 410));
            StringFormat sf = new StringFormat
            {
                LineAlignment = StringAlignment.Center,
                Alignment = StringAlignment.Center
            };

            Image buildIm = model._ResourceModel.GetImageById("IconBld", build.build_id.ToUpper());
            buildIm = GameResourceModel.MakeTransparent(ref buildIm);
            buildIm = GameResourceModel.ResizeImage(buildIm, new Size(150,150));

            using (Graphics g = Graphics.FromImage(image))
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                int posY = 80;
                int secondPartW = image.Width - offsetX * 2 - buildIm.Width - 10;

                g.DrawString(build.name_txt.value.text, nameFont, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(offsetX + 10 + buildIm.Width, posY, secondPartW, 30), sf);

                g.DrawString(build.desc_txt.value.text, descFont, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(offsetX + 10 + buildIm.Width, posY + rowH, secondPartW, 130));

                g.DrawImage(buildIm, new Rectangle(offsetX, posY, buildIm.Width, buildIm.Height));
                //g.DrawString(TranslationHelper.Instance().Tr("Level:") + " " + build.level.ToString(), descFont,
                //    new SolidBrush(StyleService.GetColor("imageFont")), new RectangleF(20, posY + 150, secondPartW, 30), sf);

                posY += 200;

                posY = DrawCostCellsH(g, offsetX + 60, posY, TranslationHelper.Instance().Tr("BuildCost:"),
                    build.cost, sf, image.Width - offsetX * 2, rowH, descFont);

                posY += rowH;

                

                posY += rowH;
                if (upgrBinding.ContainsKey(build.build_id))
                {
                    var units = upgrBinding[build.build_id];

                    int unitOffsetX = offsetX;// + 200;
                    int unitOffsetY = posY - 150;
                    int prevX = unitOffsetX;
                    

                    foreach (var unitId in units)
                    {
                        var unit = model.GetObjectT<Gunit>(unitId);
                        string unitIconId = unit.base_unit.key + "FACE";
                        if (unit.base_unit.value == null)
                            unitIconId = unit.unit_id + "FACE";

                        unitIconId = unitIconId.ToUpper();
                        int index = 0;
                        if (HasResource("Faces", unitIconId))
                        {
                            Image transfIm = GetImageById("Faces", unitIconId);
                            //transfIm = MapTileHelper.ResizeImage(transfIm, new Size(40,40);
                            int imageW = 70;
                            if (!unit.size_small)
                                imageW *= 2;

                            Rectangle imgRect = new Rectangle(prevX, unitOffsetY + 40, imageW, imageW * transfIm.Height / transfIm.Width);
                            g.DrawImage(transfIm, imgRect);
                            Rectangle borderRect = new Rectangle(imgRect.X - 5, imgRect.Y - 5, imgRect.Width + 10, imgRect.Height + 10);
                            if (unit.size_small && HasResource("Icons", "BORDERUNITSMALL"))
                            {
                                Image borderImage = GetImageById("Icons", "BORDERUNITSMALL");
                                g.DrawImage(borderImage, borderRect);
                            }
                            else if (!unit.size_small && HasResource("Icons", "BORDERUNITLARGE"))
                            {
                                Image borderImage = GetImageById("Icons", "BORDERUNITLARGE");
                                g.DrawImage(borderImage, borderRect);
                            }
                            result.clickables.Add(new ClickableArea()
                            {
                                id = unit.unit_id,
                                pos = imgRect,
                                tooltip = unit.name_txt.value.text
                            });
                            objectDesc.Add("    " + unit.name_txt.value.text + "[" + unit.unit_id + "]");
                            prevX += imageW + 15;
                            index++;
                        }
                    }
                    g.DrawString(TranslationHelper.Instance().Tr("Living in:"), nameFont, new SolidBrush(StyleService.GetColor("imageFont")),
                        new RectangleF(offsetX, unitOffsetY, 120, 30), sf);
                }
            }

            result.image = image;
            result.obj = build;
            return result;
        }
    }
}
