﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Disciples2Info
{
    public class SpellsProvider : ViewProviderBase, InfoProvider
    {
        Font nameFont = new Font("comic sans", 15, FontStyle.Bold);
        Font descFont = new Font("comic sans", 15);
        int offsetX = 180;
        int rowH = 36;

        public string GetInfoType()
        {
            return "SS";
        }

        public SpellsProvider(GameModel model_)
        {
            ComboBoxFilter typeFilter = new ComboBoxFilter();
            typeFilter.Init(model, new Size(140, 20), "<Category>",
                (entry, str) => ((Gspell)entry.item).category == (int)str);

            typeFilter.AddVariant(new FilterValue(0, "SPELL_ATTACK"));
            typeFilter.AddVariant(new FilterValue(2, "SPELL_HEAL"));
            typeFilter.AddVariant(new FilterValue(1, "SPELL_LOWER"));
            typeFilter.AddVariant(new FilterValue(3, "SPELL_BOOST"));
            typeFilter.AddVariant(new FilterValue(4, "SPELL_SUMMON"));
            typeFilter.AddVariant(new FilterValue(6, "SPELL_FOG"));
            typeFilter.AddVariant(new FilterValue(7, "SPELL_UNFOG"));
            typeFilter.AddVariant(new FilterValue(8, "SPELL_RESTORE_MOVE"));
            typeFilter.AddVariant(new FilterValue(9, "SPELL_INVISIBILITY"));
            typeFilter.AddVariant(new FilterValue(10, "SPELL_REMOVE_ROD"));
            typeFilter.AddVariant(new FilterValue(11, "SPELL_CHANGE_TERRAIN"));

            ComboBoxFilter levelFilter = new ComboBoxFilter();
            levelFilter.Init(model, new Size(80, 20), "<Level>",
                (entry, str) => ((Gspell)entry.item).level == (int)str);


            for (int i = 1; i < 6; i++)
            {
                levelFilter.AddVariant(new FilterValue(i, "Level " + i.ToString()));
            }

            filters.Add(typeFilter);
            filters.Add(levelFilter);
            model = model_;
            foreach (Gspell spell in model.GetAllT<Gspell>())
            {
                string context = spell.name_txt.value.text + " [ " + spell.level.ToString() + " ]";
                allItems.Add(new InfoEntry()
                {
                    context = context,
                    id = spell.spell_id,
                    item = spell
                });
            }
            allItems.Sort((x, y) => String.Compare(x.context, y.context));
        }
        public ViewItem GetView(IViewIntent intent)
        {
            ViewItem result = new ViewItem();
            ViewIntentBase baseIntent = intent as ViewIntentBase;
            currentId = baseIntent.id;
            Gspell spell = model.GetObjectT<Gspell>(baseIntent.id);
            Image image = StyleService.GetImage("back");
            if (image == null)
            {
                image = new Bitmap(1000, 460);
                using (Graphics gfx = Graphics.FromImage(image))
                using (SolidBrush brush = new SolidBrush(StyleService.GetColor("back")))
                {
                    gfx.FillRectangle(brush, 0, 0, image.Width, image.Height);
                }
            }
            if (spell.unit_id.value != null)
                image = GameResourceModel.ResizeImage(image, new Size(1000, 460 + 90));
            else
                image = GameResourceModel.ResizeImage(image, new Size(1000, 460));
            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

            Image spellIm = model._ResourceModel.GetImageById("IconSpel", spell.spell_id.ToUpper());
            //Image spellIm = gameModel._ResourceModel.GetImageById("Interf", "DLG_R_C_SPELL");
            spellIm = GameResourceModel.MakeTransparent(ref spellIm);
            spellIm = GameResourceModel.ResizeImage(spellIm, new Size((int)(spellIm.Width * 2), (int)(spellIm.Height * 2)));
            using (Graphics g = Graphics.FromImage(image))
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                int posY = 80;
                int secondPartW = image.Width - offsetX * 2 - spellIm.Width - 10;
                g.DrawString(spell.name_txt.value.text + " [ " + spell.level.ToString() + " ]", nameFont, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(offsetX + 10 + spellIm.Width, posY,
                    secondPartW, 30), sf);
                g.DrawString(spell.desc_txt.value.text, descFont, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(offsetX + 10 + spellIm.Width, posY + rowH * 2, secondPartW, 100));
                //g.DrawRectangle(new Pen(Color.Red), new Rectangle(offsetX, posY + unitIm.Height + 34, firstPartW, 86));
                g.DrawImage(spellIm, new Rectangle(offsetX, posY, spellIm.Width, spellIm.Height));
                posY += 180;
                posY = DrawCostCellsH(g, offsetX + 10, posY, TranslationHelper.Instance().Tr("Cast:"),
                    spell.casting_c, sf, image.Width - offsetX * 2, rowH, descFont);
                posY += rowH;
                if (spell.researchCost.Count() > 0)
                {
                    string researchCost = "";
                    for(int i = 0; i < researchCost.Count(); i++)
                    {
                        if (spell.researchCost.value[i].lord_id.value.category.value.text == "L_WARRIOR")
                        {
                            researchCost = spell.researchCost.value[i].research;
                            break;
                        }
                    }
                    if (researchCost == "")
                        researchCost = spell.researchCost.value.First().research;
                    posY = DrawCostCellsH(g, offsetX + 10, posY,
                        TranslationHelper.Instance().Tr("Reserch:"),
                        researchCost, sf, image.Width - offsetX * 2, rowH, descFont);
                }
                posY += rowH + 8;
                if (spell.unit_id.value != null)
                {
                    Gunit unit = spell.unit_id.value;
                    string unitIconId = unit.base_unit.key + "FACE";
                    if (unit.base_unit.value == null)
                        unitIconId = unit.unit_id + "FACE";
                    unitIconId = unitIconId.ToUpper();
                    if (HasResource("Faces", unitIconId))
                    {
                        Image transfIm = GetImageById("Faces", unitIconId);
                        //transfIm = MapTileHelper.ResizeImage(transfIm, new Size(40,40);
                        int imageW = 80;
                        int extraOffset = 290;
                        if (!unit.size_small)
                        {
                            imageW *= 2;
                            extraOffset = 240;
                        }
                        

                        Rectangle imgRect = new Rectangle(offsetX + extraOffset, posY, imageW, 80);
                        g.DrawImage(transfIm, imgRect);

                        result.clickables.Add(new ClickableArea()
                        {
                            id = unit.unit_id,
                            pos = imgRect,
                            tooltip = unit.name_txt.value.text
                        });
                    }

                    g.DrawString(TranslationHelper.Instance().Tr("Summons unit:"), nameFont, new SolidBrush(StyleService.GetColor("imageFont")),
                            new RectangleF(offsetX, posY, 240, 80), sf);
                }
            }

            result.image = image;
            result.obj = spell;
            return result;
        }
    }
}
