﻿using Disciples2Info;
using NevendaarTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static NevendaarTools.Logger;
using System.Net;
using System.Diagnostics;
using System.Reflection;

namespace Disciple2Info
{
    public partial class Form1 : Form
    {
        GameModel model = new GameModel();
        SettingsManager settings = SettingsManager.Instance("settings.xml");
        List<string> gameInstances = new List<string>();
        List<string> log = new List<string>();
        private void Log(LogLevel level, string str)
        {
            log.Add("[" + level + "]" + str);
        }

        void InitTranslationList()
        {
            string[] s;
            s = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory(), "*.tr");
            bool hasEng = false;
            foreach (string s2 in s)
            {
                string value = System.IO.Path.GetFileNameWithoutExtension(s2);
                langComboBox.Items.Add(value);
                if (value.OrdinalEquals("en"))
                    hasEng = true;
            }
            if (!hasEng)
                langComboBox.Items.Add("en");

            string lang = settings.Value("lang", "ru");
            if (!settings.HasValue("lang"))
                settings.SetValue("lang", lang);
            langComboBox.SelectedIndex = langComboBox.Items.IndexOf(lang);
            ReloadTranslation();
        }

        public void ReloadTranslation()
        {
            label2.Text = TranslationHelper.Instance().Tr("Game path:");
            parseGameButton.Text = TranslationHelper.Instance().Tr("Parse");
            aboutButton.Text = TranslationHelper.Instance().Tr("About");
            logButton.Text = TranslationHelper.Tr_("Log");
            buttonCheckUpdates.Text = TranslationHelper.Tr_("Updates");
        }

        public Form1()
        {
            InitializeComponent();
            //langComboBox.Height = 21;
            if (settings.HasValue("SavedPaths"))
            {
                string paths = settings.Value("SavedPaths");
                string[] pathsData = paths.Split(';');
                gameInstances.Clear();
                for(int i = 0; i < pathsData.Length; ++i)
                {
                    if (gameInstances.Contains(pathsData[i]))
                        continue;

                    gameInstances.Add(pathsData[i]);
                    pathComboBox.Items.Add(pathsData[i]);
                }
            }
            pathComboBox.Text = settings.Value("GamePath", "");
            string lang = settings.Value("lang", "ru");
            if (!settings.HasValue("lang"))
                settings.SetValue("lang", lang);

            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
            InitTranslationList();

            KeyPreview = true;
            styleSelectWidget1.LoadList(settings, null);
            StyleService.Instance().StyleChanged += ReloadStyle;
            ReloadStyle();
            Logger.Instance()._LogDelegate += Log;
            parseGameButton_Click(null, null);
        }

        private void ReloadStyle()
        {
            StyleService.ApplyStyle(this);
            this.Refresh();
            this.Invalidate();
            universalInfoView1.ReloadStyle();
        }

        private void parseGameButton_Click(object sender, EventArgs e)
        {
            if (pathComboBox.Text == "")
                return;
            if (!System.IO.File.Exists(pathComboBox.Text + "\\globals\\Tglobal.dbf"))
            {
                Logger.Log("Invalid path! File " + pathComboBox.Text + "\\globals\\Tglobal.dbf not found!!!");
                return;
            }
            universalInfoView1.Clear();
            model.Load(pathComboBox.Text, true);
            if (!gameInstances.Contains(pathComboBox.Text))
            {
                gameInstances.Add(pathComboBox.Text);
                pathComboBox.Items.Add(pathComboBox.Text);
            }

            settings.SetValue("GamePath", pathComboBox.Text);
            settings.SetValue("SavedPaths", string.Join(";", gameInstances));
            styleSelectWidget1.LoadList(settings, model);
            ReloadStyle();
            //LuaSettingsAdapter adapter = new LuaSettingsAdapter();
            //adapter.Read(pathComboBox.Text + "\\Scripts\\Settings.lua");
            //string crit = adapter.getProperty("criticalHitChance");
            //string critD = adapter.getProperty("criticalHitDamage");
            //string accur = adapter.getProperty("aiAccuracyBonus.absolute");
            //string accurEasy = adapter.getProperty("aiAccuracyBonus.easy");
            universalInfoView1.Init(settings, model);
        }

        private void selectPathButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browserDialog = new FolderBrowserDialog();
            browserDialog.SelectedPath = pathComboBox.Text;
            if (browserDialog.ShowDialog() == DialogResult.Cancel)
                return;

            pathComboBox.Text = browserDialog.SelectedPath;
            parseGameButton_Click(sender, e);
        }

        private int ParsePosInt(string value)
        {
            int res = int.Parse(value);
            if (res < -10000)
                res = 0;
            return res;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //this.Left=Screen.PrimaryScreen.Bounds.Width-this.Width;
            if (settings.HasValue("pos_X"))
            {
                this.Left = ParsePosInt(settings.Value("pos_X", "0"));
                this.Top = ParsePosInt(settings.Value("pos_Y", "0"));
                this.Width = ParsePosInt(settings.Value("pos_W", "0"));
                this.Height = ParsePosInt(settings.Value("pos_H", "0"));
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            settings.SetValue("pos_X", this.Left.ToString());
            settings.SetValue("pos_Y", this.Top.ToString());
            settings.SetValue("pos_W", this.Width.ToString());
            settings.SetValue("pos_H", this.Height.ToString());
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (universalInfoView1.ProcessKey(keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            AboutProgramBox a = new AboutProgramBox();
            a.Show();
        }

        private void logButton_Click(object sender, EventArgs e)
        {
            LogDialog dlg = new LogDialog();
            dlg.Init(log);
            dlg.Show();
        }

        private void langComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string lang = langComboBox.Text;
            settings.SetValue("lang", lang);

            TranslationHelper.Instance().LoadTranslation(lang);
            TranslationHelper.Instance().SetTranslation(lang);
        }

        private void pathComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            parseGameButton_Click(sender, e);
        }

        public static DateTime GetLinkerTime(Assembly assembly, TimeZoneInfo target = null)
        {
            var filePath = assembly.Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;

            var buffer = new byte[2048];

            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                stream.Read(buffer, 0, 2048);

            var offset = BitConverter.ToInt32(buffer, c_PeHeaderOffset);
            var secondsSince1970 = BitConverter.ToInt32(buffer, offset + c_LinkerTimestampOffset);
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            var linkTimeUtc = epoch.AddSeconds(secondsSince1970);

            var tz = target ?? TimeZoneInfo.Local;
            var localTime = TimeZoneInfo.ConvertTimeFromUtc(linkTimeUtc, tz);

            return localTime;
        }

        private string GetNews()
        {//https://docs.google.com/document/d/1F-PA7ycZHn2H-d5QBcqWvR2IRT65kCCx9FzrqP6cAHY/edit?usp=sharing
            WebClient wbc = new WebClient();
            string result = "";
            var linkTimeLocal = GetLinkerTime(Assembly.GetExecutingAssembly());
            try
            {
                wbc.DownloadFile("https://docs.google.com/feeds/download/documents/export/Export?id=" +
                     "1F-PA7ycZHn2H-d5QBcqWvR2IRT65kCCx9FzrqP6cAHY" +
                     "&exportFormat=txt", Environment.CurrentDirectory + "\\news.txt");

                
                using (StreamReader sr = File.OpenText(Environment.CurrentDirectory + "\\news.txt"))
                {
                    string s = String.Empty;
                    string lang = "";
                    string currentLang = TranslationHelper.Instance().lang();
                    DateTime tmpDate = new DateTime();
                    while ((s = sr.ReadLine()) != null)
                    {
                        if (s.StartsWith("##"))
                        {
                            tmpDate = new DateTime();
                            tmpDate = tmpDate.AddYears(int.Parse(s.Substring(8, 4)) - 1);
                            tmpDate = tmpDate.AddMonths(int.Parse(s.Substring(5, 2)) - 1);
                            tmpDate = tmpDate.AddDays(int.Parse(s.Substring(2, 2)) - 1);
                            tmpDate = tmpDate.AddHours(int.Parse(s.Substring(13, 2)));
                            tmpDate = tmpDate.AddMinutes(int.Parse(s.Substring(16, 2)));
                            if (tmpDate > linkTimeLocal)
                                result += s.Replace("##", "") + "\n";
                            continue;
                        }
                        if (s.StartsWith("#"))
                        {
                            if (tmpDate > linkTimeLocal)
                                lang = s.Replace("#", "");
                            continue;
                        }
                        if (tmpDate > linkTimeLocal)
                        {
                            if (currentLang == lang)
                                result += s.Trim() + "\n";
                            else if (lang == "en" && currentLang != "ru")
                                result += s.Trim() + "\n";
                        }
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return null;
            }
        }

        private void buttonCheckUpdates_Click(object sender, EventArgs e)
        {
            try
            {
                string news = GetNews();

                if (news != "" && news != null)
                {
                    DialogResult result = MessageBox.Show(news + "\n" + TranslationHelper.Instance().Tr("The updated version found! Download and install?"),
                        TranslationHelper.Instance().Tr("The updated version found!"), MessageBoxButtons.OKCancel);
                    if (result == DialogResult.OK)
                    {
                        Process.Start("D2InfoUpdater.exe");
                        Application.Exit();
                    }
                }
                else
                {
                    MessageBox.Show(TranslationHelper.Instance().Tr("The latest version is used!"));
                }
            }
            catch (Exception exep)
            {
                Logger.LogError(exep.Message);
                MessageBox.Show(TranslationHelper.Instance().Tr("Failed to get version info!") + exep.Message);
            }
            
        }
    }
}
