﻿using NevendaarTools;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Disciples2Info
{
    public partial class LogDialog : Form
    {
        public LogDialog()
        {
            InitializeComponent();
            StyleService.ApplyStyle(this);
            this.Refresh();
            this.Invalidate();
        }

        public void Init(List<string> logs)
        {
            this.richTextBox1.Lines = logs.ToArray();
        }
    }
}
