﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Threading;
using static System.Windows.Forms.LinkLabel;


namespace Disciples2Info
{
    public partial class UniversalInfoView : UserControl
    {
        GameModel model;
        TranslatedButton buttonClear = new TranslatedButton { Tag = "Clear", Height = 21, TextOffset = new Point(8, 3) };
        SettingsManager settings = null;
        Dispatcher dispatcher = Dispatcher.CurrentDispatcher;
        List<InfoEntry> searchResults = new List<InfoEntry>();
        List<InfoEntry> totalResults = new List<InfoEntry>();
        InfoManager manager = new InfoManager();
        string current = null;
        int selectedAreaIndex = -1;
        bool loadingState = false;
        Image currentImage = null;
        StringFormat sf = new StringFormat();
        Font baseFont = new Font("comic sans", 9);
        Image back = new Bitmap(200, 60);
        List<IViewIntent> intents = new List<IViewIntent>();
        int maxIntents = 10;
        ProgressBar bar;
        StringFilter strintFilter = new StringFilter();
        Dictionary<string, string> filterText = new Dictionary<string, string>();

        bool userAction = true;

        List<ClickableArea> areas = new List<ClickableArea>();
        List<ProviderInfoLabel> providerLabels = new List<ProviderInfoLabel>();
        Object viewObject = null;
        public UniversalInfoView()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;
            buttonClear.Click += ClearFilterValues;
            buttonClear.TranslateText();
            buttonBack.Hide();
            creatorCheckBox.Checked = false;
            creatorCheckBox_CheckedChanged(null, null);
        }

        public void OnActivateProvider(int index)
        {
            ChangeCurrentProvider(providerLabels[index].typeId, true);
        }
        public void AddProvider(InfoProvider provider)
        {
            provider.ConnectUpdateSlot(UpdateCurrentView);
            manager.RegisterProvider(provider);
            ProviderInfoLabel info = new ProviderInfoLabel(provider, providerLabels.Count());
            info.activateDelegate -= OnActivateProvider;
            info.activateDelegate += OnActivateProvider;
            providerLabels.Add(info);
            providersPanel.Controls.Add(info.label);
            provider.RegisterFilters();
            provider.FilterChanged += FilterValueChanged;
        }
        public void Init(SettingsManager settings, GameModel model_)
        {
            this.settings = settings;
            current = null;
            selectedAreaIndex = -1;
            model = model_;
            manager.UnregisterAll();
            providersPanel.Controls.Clear();
            providerLabels.Clear();
            AddProvider(new UnitsProvider(model));
            AddProvider(new ItemsProvider(model));
            AddProvider(new SpellsProvider(model));
            AddProvider(new BuildingsProvider(model));
            AddProvider(new PerksProvider(model));
            AddProvider(new SpellBookProvider(model));
            AddProvider(new EvolutionProvider(model));
            AddProvider(new MapmakerHelpProvider(model));

            strintFilter.Setup(filterTextBox);
            strintFilter.Changed -= FilterValueChanged;
            strintFilter.Changed += FilterValueChanged;

            //ReguestIntent(new ViewIntentBase("g000ig6021"));
            OnActivateProvider(0);
            ReloadStyle();
            creatorCheckBox.Checked = settings.Value("creator_mode", "False") == "True";
            creatorCheckBox_CheckedChanged(null, null);
            filterTextBox.Focus();
            filterTextBox.SelectAll();
            //ReguestIntent(new ViewIntentBase("g000uu6007"));
        }

        public void ReloadStyle()
        {
            StyleService.ApplyStyle(this);
            back = StyleService.GetImage("info");
            if (back == null)
                back = new Bitmap(20, 20);
            UpdateCurrentView();
            foreach (ProviderInfoLabel info in providerLabels)
                info.SetCurrent(info.isActive);
        }

        public void ReloadTranslation()
        {
            buttonBack.Text = TranslationHelper.Instance().Tr("Back");
            creatorCheckBox.Text = TranslationHelper.Instance().Tr("Creator mode");
            exportButton.Text = TranslationHelper.Instance().Tr("Export");
            buttonClear.TranslateText();
            listBox1_SelectedIndexChanged(null, null);
            foreach (ProviderInfoLabel label in providerLabels)
                label.ReloatTranslation();
            foreach (Control control in manager.GetControls())
                TranslateText(control);
            foreach (Control control in flowLayoutPanel2.Controls)
                TranslateCombo(control);
        }

        public static void TranslateText(Control control)
        {
            if (control is TranslatedButton button)
            {
                button.TranslateText();
            }
            else if (control.Tag is string text)
            {
                control.Text = TranslationHelper.Instance().Tr(text);
                control.Width = (int)Graphics.FromHwnd(control.Handle).MeasureString(control.Text, control.Font).Width;
                control.Width += (control is Button ? 16 : 4);
            }
        }
        public static void TranslateCombo(Control control)
        {
            if (control is ComboBox combo)
            {
                var items = new List<FilterValue>();
                foreach (object item in combo.Items)
                    if (item is FilterValue value)
                    {
                        value.Translate();
                        items.Add(value);
                    }
                if (items.Count > 0)
                {
                    int index = combo.SelectedIndex;
                    combo.DataSource = items;
                    combo.SelectedIndex = index;
                }
            }
        }

        private void ClearFilterValues(object sender, EventArgs e)
        {
            if (loadingState)
                return;
            loadingState = true;
            List<ItemFilter> filters = manager.GetItemFilters(manager.GetCurrentType());
            foreach (ItemFilter filter in filters)
            {
                filter.ResetFilter();
            }
            loadingState = false;
            UpdateFilteredList(true);
        }

        private void FilterValueChanged(object sender, EventArgs e)
        {
            if (loadingState)
                return;
            UpdateFilteredList(true);
        }

        void UpdateFilteredList(bool select)
        {
            searchResults.Clear();
            loadingState = true;
            listBox1.BeginUpdate();
            listBox1.Items.Clear();

            List<ItemFilter> filters = new List<ItemFilter>();
            filters.Add(strintFilter);
            filters.AddRange(manager.GetItemFilters(manager.GetCurrentType()));
            foreach (InfoEntry res in totalResults)
            {
                if (manager.FilterAcceptItem(res, filters))
                {
                    searchResults.Add(res);
                }
            }
            foreach (InfoEntry res in searchResults)
            {
                listBox1.Items.Add(res.context);
            }
            
            listBox1.EndUpdate();
            loadingState = false;
            if (select && listBox1.Items.Count > 0)
            {
                listBox1.SelectedIndex = 0;
                listBox1_SelectedIndexChanged(null, null);
            }
        }

        void ScrollTo(string id)
        {
            int index = 0;
            foreach (InfoEntry res in searchResults)
            {
                if (res.id.OrdinalEquals(id))
                {
                    loadingState = true;
                    listBox1.SelectedIndex = index;
                    loadingState = false;
                    return;
                }
                index++;
            }
        }

        void ChangeCurrentProvider(string newType, bool select)
        {
            string currentType = manager.GetCurrentType();
            if (currentType != null)
            {
                if (filterText.ContainsKey(currentType))
                    filterText[currentType] = filterTextBox.Text;
                else
                    filterText.Add(currentType, filterTextBox.Text);
            }
            loadingState = true;
            totalResults = manager.GetAllEntriesByType(newType);
            loadingState = false;

            manager.SelectCurrent(newType);
            flowLayoutPanel2.Controls.Clear();
            var filters = manager.GetItemFilters(manager.GetCurrentType());
            for (int i = 0; i < filters.Count; i++)
            {
                var filter = filters[i];
                //filter.Changed += FilterValueChanged;
                flowLayoutPanel2.Controls.Add(filter.GetControl());
            }
            flowLayoutPanel2.Controls.Add(buttonClear);
            StyleService.ApplyStyle(flowLayoutPanel2);

            UpdateFilteredList(select);
            flowLayoutPanel1.Controls.Clear();
            
            foreach(Control ctrl in manager.GetControls(newType))
                flowLayoutPanel1.Controls.Add(ctrl);
            StyleService.ApplyStyle(flowLayoutPanel1);

            foreach (ProviderInfoLabel info in providerLabels)
                info.SetCurrent(info.typeId == newType);
            if (filterText.ContainsKey(newType))
                filterTextBox.Text = filterText[newType];
            else
                filterTextBox.Text = "";
        }
        public void Clear()
        {
            manager.Clear();
        }
        void UpdateCurrentView()
        {
            if (manager.GetCurrentIntent() == null)
                return;
            ReguestIntent(manager.GetCurrentIntent(), false, false);
        }

        void PushIntent(IViewIntent intent)
        {
            if (intent == null)
                return;
            if (intents.Contains(intent))
                return;
            intents.Add(intent);
            if (intents.Count > maxIntents)
                intents.RemoveAt(0);
            buttonBack.Visible = intents.Count > 0;
        }

        bool PopIntent()
        {
            if (intents.Count < 1)
                return false;
            IViewIntent intent = intents.Last();
            intents.Remove(intent);
            ReguestIntent(intent, false, false);
            buttonBack.Visible = intents.Count > 0;
            return true;
        }

        void ReguestIntent(IViewIntent intent, bool withScroll = false, bool savePrev = true)
        {
            if (!manager.HasProvider(intent.GetProviderType()))
                return;
            if (savePrev)
                PushIntent(manager.GetCurrentIntent());

            string type = manager.GetCurrentType();
            ViewItem viewItem = manager.GetView(intent);
            if (viewItem.image != null)
                currentImage = viewItem.image.Clone() as Image;
            string newType = manager.GetCurrentType();
            ViewIntentBase baseIntent = intent as ViewIntentBase;
            current = baseIntent.id.ToLower();
            if (type != newType)
            {
                ChangeCurrentProvider(newType, false);
            }
            selectedAreaIndex = -1;
            pictureBox1.Image = viewItem.image;
            idTextBox.Text = current;
            areas = viewItem.clickables;
            viewObject = viewItem.obj;
            UpdateProps();
            if (withScroll || type != newType)
                ScrollTo(current);
        }

        private void UpdateProps()
        {
            
            propertiesView.BeginUpdate();
            propertiesView.Nodes.Clear();
            if (viewObject != null && creatorCheckBox.Checked)
            {
                TreeNode rootNode = new TreeNode(viewObject.GetType().Name + idTextBox.Text);
                FillProperties(viewObject, rootNode);
                foreach (TreeNode node in rootNode.Nodes)
                    propertiesView.Nodes.Add(node);
            }
            propertiesView.EndUpdate();
        }

        protected void FillProperties(object obj, TreeNode parent, List<string> tmp = null)
        {
            if (tmp == null)
                tmp = new List<string>();
            
            if (obj == null || obj.GetType().GetFields().Count() < 1)
                return;
            string id = obj.GetType().GetFields()[0].GetValue(obj).ToString();
            if (tmp.Contains(id + obj.GetType().Name))
                return;
            string space = "";
            for (int i = 0; i < tmp.Count; ++i)
            {
                space += "  ";
            }
            List<string> tmp2 = new List<string>();
            foreach (string data in tmp)
                tmp2.Add(data);
            tmp2.Add(id + obj.GetType().Name);

            foreach (FieldInfo field in obj.GetType().GetFields())
            {
                if (typeof(IGameField).IsAssignableFrom(field.FieldType))
                {
                    IGameField gameField = field.GetValue(obj) as IGameField;
                    if (gameField.GetKey() == null)
                    {
                        TreeNode nullNode = new TreeNode(field.Name.PadRight(16, ' ') + "null");
                        parent.Nodes.Add(nullNode);
                        continue;
                    }
                    string data = "";
                    if (gameField.GetKey().GetType().Name.OrdinalEquals("int32"))
                        data = Convert.ToInt32(gameField.GetKey()).ToString();
                    else
                        data = gameField.GetKey().ToString();
                    TreeNode newNode = new TreeNode(field.Name.PadRight(16, ' ') + data);
                    newNode.Name = data;
                    FillProperties(gameField.GetValue(), newNode, tmp2);
                    if (newNode.Nodes.Count < 5)
                        newNode.ExpandAll();
                    parent.Nodes.Add(newNode);

                    continue;
                }
                if (typeof(IGameFieldList).IsAssignableFrom(field.FieldType))
                {
                    IGameFieldList gameField = field.GetValue(obj) as IGameFieldList;
                    System.Collections.IList list = (System.Collections.IList)gameField.GetValue();
                    string data = "";
                    if (gameField.GetKey().GetType().Name.OrdinalEquals("int32"))
                        data = Convert.ToInt32(gameField.GetKey()).ToString();
                    else
                        data = gameField.GetKey().ToString();
                    TreeNode newNode = new TreeNode(field.Name.PadRight(16, ' '));

                    for (int i = 0; i < list.Count; ++i)
                    {
                        string name = list[i].ToString();
                        if (name == "" || name == null)
                            name = (field.Name + "_" + i.ToString());
                        name = ViewProviderBase.RemoveStyles(name);
                        TreeNode indexNode = new TreeNode(name);
                        FillProperties(list[i], indexNode, tmp2);
                        if (indexNode.Nodes.Count < 5)
                            indexNode.Expand(); 
                        newNode.Nodes.Add(indexNode);
                    }
                    parent.Nodes.Add(newNode);

                    continue;
                }
                if (field.FieldType.IsGenericType && field.FieldType.GetGenericTypeDefinition() == typeof(List<>) ||
                        field.FieldType.IsArray)
                {
                    TreeNode newNode = new TreeNode(field.Name + "(list)");

                    System.Collections.IList list = (System.Collections.IList)field.GetValue(obj);

                    for (int i = 0; i < list.Count; ++i)
                    {
                        FillProperties(list[i], newNode, tmp2);
                    }
                    parent.Nodes.Add(newNode);
                    continue;
                }
                string fieldType = field.FieldType.Name.ToLower();
                switch (fieldType)
                {
                    case "boolean":
                        string boolData = Convert.ToBoolean(field.GetValue(obj)).ToString();
                        TreeNode newNode = new TreeNode(field.Name.PadRight(16, ' ') + boolData);
                        newNode.Name = boolData;
                        newNode.Tag = obj;
                        parent.Nodes.Add(newNode);
                        break;
                    case "int32":
                        string intData = Convert.ToInt32(field.GetValue(obj)).ToString();
                        TreeNode newIntNode = new TreeNode(field.Name.PadRight(16, ' ') + intData);
                        newIntNode.Name = intData;
                        newIntNode.Tag = obj;
                        parent.Nodes.Add(newIntNode);
                        break;

                    case "double":
                        break;

                    case "string":
                        string stringData = Convert.ToString(field.GetValue(obj));
                        TreeNode newstrNode = new TreeNode(field.Name.PadRight(16, ' ') + stringData);
                        newstrNode.Name = stringData;
                        newstrNode.Tag = obj;
                        parent.Nodes.Add(newstrNode);
                        break;
                }

            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (loadingState)
                return;
            int index = listBox1.SelectedIndex;
            
            if (index < 0 || index > searchResults.Count)
                return;
            if (userAction)
                intents.Clear();
            buttonBack.Visible = false;
            ReguestIntent(new ViewIntentBase(searchResults[index].id), false, !userAction);
        }

        private void ConvertCoordinates(PictureBox pic, out int X0, out int Y0, int x, int y)
        {
            if (pic.Image == null)
            {
                X0 = 0;
                Y0 = 0;
                return;
            }
            int pic_hgt = pic.ClientSize.Height;
            int pic_wid = pic.ClientSize.Width;
            int img_hgt = pic.Image.Height;
            int img_wid = pic.Image.Width;

            X0 = x;
            Y0 = y;
            switch (pic.SizeMode)
            {
                case PictureBoxSizeMode.AutoSize:
                case PictureBoxSizeMode.Normal:
                    // These are okay. Leave them alone.
                    break;
                case PictureBoxSizeMode.CenterImage:
                    X0 = x - (pic_wid - img_wid) / 2;
                    Y0 = y - (pic_hgt - img_hgt) / 2;
                    break;
                case PictureBoxSizeMode.StretchImage:
                    X0 = (int)(img_wid * x / (float)pic_wid);
                    Y0 = (int)(img_hgt * y / (float)pic_hgt);
                    break;
                case PictureBoxSizeMode.Zoom:
                    float pic_aspect = pic_wid / (float)pic_hgt;
                    float img_aspect = img_wid / (float)img_hgt;
                    if (pic_aspect > img_aspect)
                    {
                        // The PictureBox is wider/shorter than the image.
                        Y0 = (int)(img_hgt * y / (float)pic_hgt);

                        // The image fills the height of the PictureBox.
                        // Get its width.
                        float scaled_width = img_wid * pic_hgt / img_hgt;
                        float dx = (pic_wid - scaled_width) / 2;
                        X0 = (int)((x - dx) * img_hgt / (float)pic_hgt);
                    }
                    else
                    {
                        // The PictureBox is taller/thinner than the image.
                        X0 = (int)(img_wid * x / (float)pic_wid);

                        // The image fills the height of the PictureBox.
                        // Get its height.
                        float scaled_height = img_hgt * pic_wid / img_wid;
                        float dy = (pic_hgt - scaled_height) / 2;
                        Y0 = (int)((y - dy) * img_wid / pic_wid);
                    }
                    break;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var mouseEventArgs = e as MouseEventArgs;
            if (mouseEventArgs != null)
            {
                int posX, posY;
                ConvertCoordinates(pictureBox1,
                    out posX, out posY, mouseEventArgs.X, mouseEventArgs.Y);
                Console.WriteLine(new Point(mouseEventArgs.X, mouseEventArgs.Y).ToString() + "==>" +
                    new Point(posX, posY).ToString());
                foreach (ClickableArea area in areas)
                {
                    if (area.pos.Contains(new Point(posX, posY)))
                    {
                        userAction = false;
                        bool switchCur = mouseEventArgs.Button == MouseButtons.Right;
                        ReguestIntent(new ViewIntentBase(area.id), switchCur, !switchCur);
                        userAction = true;
                        return;
                    }
                }
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            var mouseEventArgs = e as MouseEventArgs;
            if (mouseEventArgs != null)
            {
                int posX, posY;
                ConvertCoordinates(pictureBox1,
                    out posX, out posY, mouseEventArgs.X, mouseEventArgs.Y);
                int index = 0;
                foreach (ClickableArea area in areas)
                {
                    if (area.pos.Contains(new Point(posX, posY)))
                    {
                        if (index != selectedAreaIndex)
                        {
                            Image image = pictureBox1.Image;
                            using (Graphics g = Graphics.FromImage(image))
                            {
                                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                                g.DrawRectangle(new Pen(new SolidBrush(StyleService.GetColor("imageFont")), 2), area.pos);
                                if (area.tooltip != "")
                                {
                                    int tooltipH = 32;
                                    int tooltipW = 24 + area.tooltip.Length * 8;
                                    if (tooltipW < area.recommendMinW)
                                        tooltipW = area.recommendMinW;
                                    if (tooltipW > area.recommendMaxW)
                                    {
                                        tooltipH = tooltipH * (int) Math.Ceiling(tooltipW / (double)area.recommendMaxW);
                                        tooltipW = area.recommendMaxW;
                                    }
                                    Rectangle rect = new Rectangle((area.pos.X + area.pos.Width / 2) - tooltipW / 2,
                                        area.pos.Y - tooltipH, tooltipW, tooltipH);
                                    if (rect.X < 0)
                                        rect.X = 5;
                                    if (rect.Y < 0)
                                        rect.Y = 5;
                                    if (rect.X + rect.Width > image.Width)
                                        rect.X = image.Width - rect.Width - 5;
                                    Rectangle textRect = new Rectangle(rect.X + (int)(rect.Width * 0.1), rect.Y + 5, (int)(rect.Width * 0.8), rect.Height - 10);
                                    g.DrawImage(back, rect);
                                    g.DrawString(area.tooltip, baseFont, new SolidBrush(StyleService.GetColor("imageFont")), textRect, sf);
                                }
                            }
                            pictureBox1.Image = image;
                            selectedAreaIndex = index;
                        }
                        return;
                    }
                    index++;
                }
                if (currentImage != null && selectedAreaIndex != -1)
                {
                    selectedAreaIndex = -1;
                    pictureBox1.Image = currentImage.Clone() as Image;
                }
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            PopIntent();
        }

        public bool ProcessKey(Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.F))
            {
                filterTextBox.Focus();
                filterTextBox.SelectAll();
                return true;
            }
            if (keyData == (Keys.Escape))
            {
                return PopIntent();
            }
            else if (keyData == (Keys.Up))
            {
                int index = listBox1.SelectedIndex;
                if (index > 0)
                    listBox1.SelectedIndex = index - 1;
                return true;
            }
            else if (keyData == (Keys.Down))
            {
                int index = listBox1.SelectedIndex;
                if (index < listBox1.Items.Count - 1)
                    listBox1.SelectedIndex = index + 1;
                return true;
            }
            //else if (keyData == (Keys.Right))
            //{
            //    int index = listBox1.SelectedIndex;
            //    if (index > 0)
            //        listBox1.SelectedIndex = index - 1;
            //    return true;
            //}
            //else if (keyData == (Keys.Left))
            //{
            //    int index = listBox1.SelectedIndex;
            //    if (index < listBox1.Items.Count - 1)
            //        listBox1.SelectedIndex = index + 1;
            //    return true;
            //}
            return false;
        }
        private void UniversalInfoView_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void creatorCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            propertiesView.Visible = creatorCheckBox.Checked;
            if (!creatorCheckBox.Checked)
                pictureBox1.Width = Width - pictureBox1.Left;
            else
                pictureBox1.Width = Width - pictureBox1.Left - propertiesView.Width;
            if (settings != null)
                settings.SetValue("creator_mode", creatorCheckBox.Checked? "True" : "False");
            listBox1_SelectedIndexChanged(null, null);
        }

        private void propertiesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int index = propertiesView.Selected;

            //if (index < 0 || index > props.Count)
            //    return;
            //if (props[index].value != null && props[index].value != "")
            //    Clipboard.SetText(props[index].value);
        }

        private void propertiesView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 3) //ctrl + c
            {
                if (propertiesView.SelectedNode != null)
                {
                    if (propertiesView.SelectedNode.Name != null && propertiesView.SelectedNode.Name != "")
                        Clipboard.SetText(propertiesView.SelectedNode.Name);
                    else if (propertiesView.SelectedNode.Text != null && propertiesView.SelectedNode.Text != "")
                        Clipboard.SetText(propertiesView.SelectedNode.Text);
                    e.Handled = true;

                }
            }
        }

        private void propertiesView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (propertiesView.SelectedNode != null)
            {
                if (propertiesView.SelectedNode.Name != null && propertiesView.SelectedNode.Name != "")
                    Clipboard.SetText(propertiesView.SelectedNode.Name);
                else if (propertiesView.SelectedNode.Text != null && propertiesView.SelectedNode.Text != "")
                    Clipboard.SetText(propertiesView.SelectedNode.Text);

            }
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browserDialog = new FolderBrowserDialog();
            if (settings.HasValue("last_export_dir"))
                browserDialog.SelectedPath = settings.Value("last_export_dir");
            if (browserDialog.ShowDialog() == DialogResult.Cancel)
                return;

            settings.SetValue("last_export_dir", browserDialog.SelectedPath);
            
            List<InfoEntry> result = manager.GetAllEntriesByType(manager.GetCurrentType());
            bar = new ProgressBar();
            bar.Minimum = 0;
            bar.Maximum = result.Count;
            bar.Value = 0;
            bar.Step = 1;
            bar.Width = exportButton.Width;
            bar.Height = 14;
            bar.Location = new System.Drawing.Point(exportButton.Left, this.Height - 16);
            this.Controls.Add(bar);
            bar.Show();
            this.Parent.Enabled = false;//TODO: make it more safe
            new Thread(() => {
                foreach (InfoEntry entry in result)
                {
                    ViewItem viewItem = manager.GetView(new ViewIntentBase(entry.id) { ignoreSettings = true, showHelp = false });
                    string filename = browserDialog.SelectedPath + "/";
                    filename += Helper.ReplaceInvalidFilenameChars(entry.id + "_" + entry.context + ".png");
                    viewItem.image.Save(filename);
                    if (viewItem.desc != null && viewItem.desc.Count > 0)
                    {
                        string textFilename = browserDialog.SelectedPath + "/" + Helper.ReplaceInvalidFilenameChars(entry.id + "_" + entry.context + ".txt");
                        using (StreamWriter writer = new StreamWriter(textFilename))
                        {
                            foreach (string line in viewItem.desc)
                            {
                                writer.WriteLine(line);
                            }
                        }
                    }
                    this.dispatcher.Invoke(
                        new System.Action(() => this.bar.PerformStep())
                    );                    
                }
                this.dispatcher.Invoke(
                        new System.Action(() => { this.Controls.Remove(bar); bar = null; this.Parent.Enabled = true; })
                    ); 
            }).Start();
        }

        private void buttonFilters_Click(object sender, EventArgs e)
        {
            bool hasFilters = false;
            List<ItemFilter> filters = new List<ItemFilter>();
            //filters.Add(strintFilter);
            filters.AddRange(manager.GetItemFilters(manager.GetCurrentType()));
            foreach (ItemFilter filter in filters)
                if (filter.HasActiveFilter())
                {
                    hasFilters = true;
                    break;
                }
            if (buttonFilters.Text.OrdinalEquals("⁺"))
            {
                buttonFilters.Text = "⁻";
                flowLayoutPanel2.Visible = true;
            }
            else
            {
                buttonFilters.Text = "⁺";
                flowLayoutPanel2.Visible = false;
            }
            if (hasFilters)
            {
                foreach (ItemFilter filter in filters)
                    filter.ResetFilter();
                UpdateFilteredList(true);
            }
        }
    }
}
