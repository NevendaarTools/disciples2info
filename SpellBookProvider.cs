﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Disciples2Info
{
    class SpellBookProvider : ViewProviderBase, InfoProvider
    {
        Font nameFont = new Font("comic sans", 15, FontStyle.Bold);
        Font descFont = new Font("comic sans", 15);
        Dictionary<string, List<Gspell>> mapping = new Dictionary<string, List<Gspell>>();

        public string GetInfoType()
        {
            return "SB";
        }

        public SpellBookProvider(GameModel model_)
        {
            mapping.Clear();
            model = model_;
            foreach (Gspell spell in model.GetAllT<Gspell>())
            {
                string context = TranslationHelper.Instance().Tr("Undefined");
                GspellR spellLord = null;
                if (spell.researchCost.value.Count > 0) 
                    spellLord = spell.researchCost.value.First();
                if (spellLord != null)
                {
                    Glord lord = spellLord.lord_id.value;
                    if (lord != null)
                    {
                        Grace race = lord.race_id.value;
                        context = race.name_txt.value.text;
                    }
                }
                context += " [ " + spell.level.ToString() + " ]";
                if (!mapping.ContainsKey(context))
                    mapping.Add(context, new List<Gspell>());
                mapping[context].Add(spell);
                
            }
            foreach (string context in mapping.Keys)
            {
                allItems.Add(new InfoEntry()
                {
                    context = context,
                    id = "0000SB" + context,
                    item = null
                });
            }
            allItems.Sort((x, y) => String.Compare(x.context, y.context));
        }
        public ViewItem GetView(IViewIntent intent)
        {
            ViewItem result = new ViewItem();
            ViewIntentBase baseIntent = intent as ViewIntentBase;
            currentId = baseIntent.id.Substring(6);

            Image image =  StyleService.GetImage("back");
            int maxSpellsCount = 8;
            int w = 1400;
            int offsetX = 225;
            int offsetY = 200;
            int rowH = 36;
            int h = 0;
            if (mapping[currentId].Count > maxSpellsCount)
                h = maxSpellsCount * 170 + offsetY * 2+ 60;
            else
                h = mapping[currentId].Count * 170 + offsetY * 2 + 60;


            if (mapping[currentId].Count > maxSpellsCount)
            {
                w *= 1 + (mapping[currentId].Count - 1) / maxSpellsCount;
                //h = (int)( h / (0.45 + (mapping[currentId].Count - 1) / maxSpellsCount));
                offsetX *= 1 + (mapping[currentId].Count - 1) / maxSpellsCount;
                //offsetY *= 1 + (mapping[currentId].Count - 1) / maxSpellsCount;
            }
            if (image == null)
            {
                offsetX = 10;
                offsetY = 10;
                w -= 450 + (mapping[currentId].Count - 1) / maxSpellsCount * 300;
                h -= 300;
                image = new Bitmap(w, h);
                using (Graphics gfx = Graphics.FromImage(image))
                using (SolidBrush brush = new SolidBrush(StyleService.GetColor("back")))
                {
                    gfx.FillRectangle(brush, 0, 0, image.Width, image.Height);
                }
            }
            image = GameResourceModel.ResizeImage(image, new Size(w, h));


            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

            using (Graphics g = Graphics.FromImage(image))
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                for (int i = 0; i < mapping[currentId].Count; ++i)
                {
                    Gspell spell = mapping[currentId][i];
                    Rectangle area = new Rectangle(offsetX + 920 * ( i / maxSpellsCount),
                        offsetY + (i % maxSpellsCount) * (170 + 10), 910, 170);

                    //Image backIm = StyleService.GetImage("back").Clone() as Image;
                    //backIm = GameResourceModel.ResizeImage(backIm, new Size(area.Width, area.Height)); 
                    //g.DrawImage(backIm, area);

                    Image spellIm = model._ResourceModel.GetImageById("IconSpel", spell.spell_id.ToUpper());
                    spellIm = GameResourceModel.MakeTransparent(ref spellIm);
                    g.DrawImage(spellIm, new Rectangle(area.Left, area.Top + (area.Height - spellIm.Height) / 2, spellIm.Width, spellIm.Height));
                    Rectangle nameRect = new Rectangle(area.Left + spellIm.Width + 20, area.Top, area.Width - spellIm.Width - 40, rowH);
                    g.DrawString(spell.name_txt.value.text + " [ " + spell.level.ToString() + " ]", nameFont, new SolidBrush(StyleService.GetColor("imageFont")),
                        nameRect, sf);

                    Rectangle textRect = new Rectangle(area.Left + spellIm.Width + 20, area.Top + rowH, area.Width - spellIm.Width - 40, area.Height);
                    g.DrawString(spell.desc_txt.value.text, descFont, new SolidBrush(StyleService.GetColor("imageFont")), textRect);
                    int posY = area.Top + 45;

                    
                    if (spell.researchCost.Count() > 0)
                    {
                        string researchCost = "";
                        for (int lordIndex = 0; lordIndex < spell.researchCost.Count(); lordIndex++)
                        {
                            if (spell.researchCost.value[lordIndex].lord_id.value.category.value.text == "L_WARRIOR")
                            {
                                researchCost = spell.researchCost.value[lordIndex].research;
                                break;
                            }
                        }
                        if (researchCost == "")
                            researchCost = spell.researchCost.value.First().research;
                        posY = DrawCostCellsH(g, textRect.Left + 200, posY, "",
                            researchCost, sf, image.Width - offsetX * 2, rowH, descFont);
                        g.DrawString(TranslationHelper.Instance().Tr("Reserch:"), descFont, new SolidBrush(StyleService.GetColor("imageFont")),
                        new RectangleF(textRect.Left, posY, 200, rowH));
                    }

                    posY = DrawCostCellsH(g, textRect.Left + 200, posY, "",
                        spell.casting_c, sf, image.Width - offsetX * 2, rowH, descFont);
                    g.DrawString(TranslationHelper.Instance().Tr("Cast:"), descFont, new SolidBrush(StyleService.GetColor("imageFont")),
                        new RectangleF(textRect.Left, posY, 200, rowH));
                    result.clickables.Add(new ClickableArea()
                    {
                        id = spell.spell_id,
                        pos = area,
                        tooltip = ""
                    });
                }
            }
                
            result.image = image;
            result.obj = null;
            return result;
        }


        public bool FilterAcceptItem(InfoEntry entry, List<FilterValue> filters)
        {
            string filter = (string)filters[0].GetValue();
            return filter == "" || entry.context.OrdinalContains(filter) || entry.id.OrdinalEquals(filter);
        }
    }
}
