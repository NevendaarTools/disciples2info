﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Disciples2Info
{
    public class ProviderInfoLabel
    {
        public delegate void ActivateDelegate(int labelIndex);
        public ActivateDelegate activateDelegate;

        public string typeId;
        public Label label;
        public int index;
        public bool isActive = false;
        public ProviderInfoLabel(InfoProvider provider, int index_)
        {
            typeId = provider.GetInfoType();
            label = new Label();
            label.Text = TranslationHelper.Instance().Tr(typeId + "_name");
            //label.AutoSize = true;
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.Height = 20;
            label.Width = TextRenderer.MeasureText(label.Text, label.Font).Width + 10;
            if (label.Width < 90)
                label.Width = 90;
            label.BorderStyle = BorderStyle.FixedSingle;
            label.Click += OnClicked;
            index = index_;
        }
        public void OnClicked(object sender, EventArgs e)
        {
            if (isActive)
                return;
            activateDelegate?.Invoke(index);
        }

        public void SetCurrent(bool active)
        {
            isActive = active;
            if (active)
                label.BackColor = StyleService.GetColor("selected");
            else
                label.BackColor = StyleService.GetColor("back");
        }

        public void ReloatTranslation()
        {
            label.Text = TranslationHelper.Instance().Tr(typeId + "_name");
            label.Width = TextRenderer.MeasureText(label.Text, label.Font).Width + 10;
            if (label.Width < 90)
                label.Width = 90;
        }
    }
}
