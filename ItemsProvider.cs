﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Disciples2Info
{
    public class ItemsProvider : ViewProviderBase, InfoProvider
    {
        Font nameFont = new Font("comic sans", 15, FontStyle.Bold);
        Font descFont = new Font("comic sans", 15);
        int offsetX = 180;
        int rowH = 36;

        string InfoProvider.GetInfoType()
        {
            return "IG";
        }

        protected string ItemTarget(Gattack skill)
        {
            if (skill.reach.key == 1)
                return TranslationHelper.Instance().Tr("all battlfield");
            if (skill.reach.key == 2)
                return TranslationHelper.Instance().Tr("single");
            if (skill.reach.value.target_txt.value != null)
            {
                return skill.reach.value.reach_txt.value.text;
            }
            return skill.reach.key.ToString() + " " + skill.reach.value.ToString();
        }
        public ItemsProvider(GameModel model_)
        {
            ComboBoxFilter typeFilter = new ComboBoxFilter();
            typeFilter.Init(model, new Size(107, 20), "<Category>",
                (entry, str) => ((GItem)entry.item).item_cat == (int)str);
             
            typeFilter.AddVariant(new FilterValue(0, "ITEM_ARMOR"));
            typeFilter.AddVariant(new FilterValue(2, "ITEM_WEAPON"));
            typeFilter.AddVariant(new FilterValue(1, "ITEM_JEWEL"));
            typeFilter.AddVariant(new FilterValue(3, "ITEM_BANNER"));
            typeFilter.AddVariant(new FilterValue(4, "ITEM_BOOST"));
            typeFilter.AddVariant(new FilterValue(5, "ITEM_HEAL"));
            typeFilter.AddVariant(new FilterValue(6, "ITEM_REVIVE"));
            typeFilter.AddVariant(new FilterValue(7, "ITEM_PERMANENT"));
            typeFilter.AddVariant(new FilterValue(8, "ITEM_SCROLL"));
            typeFilter.AddVariant(new FilterValue(9, "ITEM_WAND"));
            typeFilter.AddVariant(new FilterValue(10, "ITEM_VALUABLE"));
            typeFilter.AddVariant(new FilterValue(11, "ITEM_ORB"));
            typeFilter.AddVariant(new FilterValue(12, "ITEM_TALISMAN"));
            typeFilter.AddVariant(new FilterValue(13, "ITEM_TRAVEL"));
            typeFilter.AddVariant(new FilterValue(14, "ITEM_SPECIAL"));
            filters.Add(typeFilter);
            
            LimitFilter itemCostFilter = new LimitFilter();
            itemCostFilter.Init(model, new Size(95, 74), "Cost",
                (entry, str) => { return true; });
            itemCostFilter._getDelegate = (entry) =>
            {
                int result = CostHelper.G(((GItem)entry.item).value);
                if (((GItem)entry.item).item_cat == (int)GItem.Category.Talisman)
                {
                    GVars vars = model.GetAllT<GVars>()[0];
                    result *= vars.talis_chrg;
                }
                return result;
            };
            filters.Add(itemCostFilter);
            model = model_;
            foreach (GItem item in model.GetAllT<GItem>())
            {
                string context = item.name_txt.value.text;
                if (context == null)
                    context = item.name_txt.key;
                allItems.Add(new InfoEntry()
                {
                    context = RemoveStyles(context),
                    id = item.item_id,
                    item = item
                });
            }
            allItems.Sort((x, y) => String.Compare(x.context, y.context));
        }
        public ViewItem GetView(IViewIntent intent)
        {
            ViewItem result = new ViewItem();
            ViewIntentBase baseIntent = intent as ViewIntentBase;
            currentId = baseIntent.id;
            GItem item = model.GetObjectT<GItem>(baseIntent.id.ToLower());
            if (item == null)
            {
                Logger.LogError("Failed to get item:" + baseIntent.id);
                return result;
            }
            Image image = StyleService.GetImage("back");
            if (image == null)
            {
                image = new Bitmap(1000, 460);
                using (Graphics gfx = Graphics.FromImage(image))
                using (SolidBrush brush = new SolidBrush(StyleService.GetColor("back")))
                {
                    gfx.FillRectangle(brush, 0, 0, image.Width, image.Height);
                }
            }
            else
                image = GameResourceModel.ResizeImage(image, new Size(1000, 460));
            GVars vars = model.GetAllT<GVars>()[0];

            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

            Image itemIm;
            if (HasResource("IconItem", item.item_id.ToUpper()))
                itemIm = GetImageById("IconItem", item.item_id.ToUpper());
            else if (item.item_cat == 8)
                itemIm = GetImageById("IconItem", "SCROLLHU");
            else
                itemIm = new Bitmap(10, 10);
            itemIm = GameResourceModel.ResizeImage(itemIm, new Size(itemIm.Width * 2, itemIm.Height * 2));
            var textBrush = new SolidBrush(StyleService.GetColor("imageFont"));
            using (Graphics g = Graphics.FromImage(image))
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                int posY = 80;
                int secondPartW = image.Width - offsetX * 2 - itemIm.Width - 10;
                
                Color rectColor = FetchBackColor(item.name_txt.value.text, Color.Empty);
                if (rectColor != Color.Empty)
                    g.FillRectangle(new SolidBrush(rectColor), new RectangleF(offsetX + 10 + itemIm.Width, posY, secondPartW, 30));

                g.DrawString(RemoveStyles(item.name_txt.value.text), nameFont,
                    new SolidBrush(FetchColor(item.name_txt.value.text, StyleService.GetColor("imageFont"))),
                    //textBrush,
                    new RectangleF(offsetX + 10 + itemIm.Width, posY,
                    secondPartW, 30), sf);
                g.DrawString(RemoveStyles(item.desc_txt.value.text.Replace("\\n", "\n")), descFont, textBrush,
                    new RectangleF(offsetX + 10 + itemIm.Width, posY + rowH * 2, secondPartW, 160));
                //g.DrawRectangle(new Pen(Color.Red), new Rectangle(offsetX, posY + unitIm.Height + 34, firstPartW, 86));
                g.DrawImage(itemIm, new Rectangle(offsetX, posY, itemIm.Width, itemIm.Height));
                posY += 180;
                int scale = 1;
                if (item.item_cat == (int)GItem.Category.Talisman)
                    scale = vars.talis_chrg;
                DrawCostCellsH(g, offsetX + 10, posY, "",
                    item.value, sf, image.Width - offsetX * 2, rowH, descFont, scale);
                posY += rowH * 2 + 5;
                if (item.attack_id.value != null)
                {
                    int oldY = posY;
                    Gattack attack = item.attack_id.value;
                    string attackClass = attack.clasS.value.text;
                    if (transfCats.Contains(attackClass))
                        attackClass += ":";
                    DrawSkillTransf(g, attack, offsetX + 170, image.Width - offsetX * 2, image.Width, ref posY, ref result, offsetX, 0);
                    string attackType = TranslationHelper.Instance().Tr(attackClass);
                    if (attack.clasS.value.text == "L_BOOST_DAMAGE")
                    {
                        attackType += " " + (attack.level * 25).ToString() + "%";
                    }
                    string attackTarget = "";
                    if (item.item_cat == 12 || item.item_cat == 11)//talisman sphere 
                        attackTarget = ItemTarget(attack);
                    int attackHeight = (int)g.MeasureString(attackType, nameFont, 170).Height;
                    int targetHeight = (int)g.MeasureString(attackTarget, descFont, 170).Height;
                    if (oldY + attackHeight + targetHeight >= image.Height)
                        oldY -= (attackHeight + targetHeight) / 2;
                    g.DrawString(attackType, nameFont, textBrush, new RectangleF(offsetX, oldY, 170, attackHeight), sf);
                    g.DrawString(attackTarget, descFont, textBrush, new RectangleF(offsetX, oldY + attackHeight, 170, targetHeight), sf);
                }
                if (item.spell_id.value != null)
                {
                    Gspell spell = item.spell_id.value;
                    Image spellIm = model._ResourceModel.GetImageById("IconSpel", item.spell_id.key.ToUpper());
                    Rectangle spellRect = new Rectangle(offsetX + 240, posY, 80, 80);
                    g.DrawImage(spellIm, spellRect);
                    Rectangle activeRect = new Rectangle(offsetX + 230, posY, 180 + 90, 80);
                    result.clickables.Add(new ClickableArea()
                    {
                        id = spell.spell_id,
                        pos = activeRect,
                        tooltip = spell.name_txt.value.text
                    });
                    g.DrawString(TranslationHelper.Instance().Tr("Contain spell:" ), nameFont, textBrush,
                            new RectangleF(offsetX - 15, posY, 240, rowH), sf);
                    g.DrawString(TranslationHelper.Instance().Tr("Сonsumes mana:") +
                        TranslationHelper.Instance().Tr(item.item_cat == 9?"Yes" : "No"), nameFont, textBrush,
                            new RectangleF(offsetX - 15, posY + rowH, 240, rowH), sf);
                    g.DrawString(spell.name_txt.value.text, nameFont, textBrush,
                            new RectangleF(offsetX + 230 + 90, posY, 170, rowH * 2), sf);
                    if (spell.unit_id.value != null)
                    {
                        Gunit unit = spell.unit_id.value;
                        string unitIconId = unit.base_unit.key + "FACE";
                        if (unit.base_unit.value == null)
                            unitIconId = unit.unit_id + "FACE";
                        unitIconId = unitIconId.ToUpper();
                        if (HasResource("Faces", unitIconId))
                        {
                            Image transfIm = GetImageById("Faces", unitIconId);
                            int imageW = 50;
                            int extraOffset = 230 + 280;
                            if (!unit.size_small)
                            {
                                imageW *= 2;
                            }

                            Rectangle imgRect = new Rectangle(offsetX + extraOffset, posY + 15, imageW, 50);
                            g.DrawImage(transfIm, imgRect);

                            result.clickables.Add(new ClickableArea()
                            {
                                id = unit.unit_id,
                                pos = imgRect,
                                tooltip = unit.name_txt.value.text
                            });
                        }
                    }
                }
            }

            result.image = image;
            result.obj = item; 
            return result;
        }
    }
}
