﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace Disciples2Info
{
    public class UnitsProvider : ViewProviderBase, InfoProvider
    {
        public static List<string> SpecialCats()
        {
            return new List<string>() { "L_LEADER", "L_SUMMON", "L_ILLUSION", "L_NOBLE", "L_GUARDIAN" };
        }
        List<string> specialCats = SpecialCats();

        Dictionary<string, List<string>> upgrBinding = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
        NumericUpDown levelUpDown = new NumericUpDown();
        Label label = new Label();

        NumericUpDown expUpDown = new NumericUpDown();
        Label expLabel = new Label();
        TranslatedButton giveExpButton = new TranslatedButton();

        Font nameFont = new Font("comic sans", 9, FontStyle.Bold);
        Font unitNameFont = new Font("comic sans", 11, FontStyle.Bold);
        Font unitCatFont = new Font("comic sans", 10, FontStyle.Bold);
        Font baseFont = new Font("comic sans", 9);
        Font extraFont = new Font("comic sans", 9);
        Font immunFont = new Font("comic sans", 8);
        int nameW = 120;
        int offsetX = 140;
        int offsetY = 80;
        int rowH = 18;
        bool inAction = false;
        string unitId;
        string InfoProvider.GetInfoType()
        {
            return "UU";
        }

        int calculateEffectiveHp(int hp, int armor)
        {
            if (armor < 0) armor = 0;
            if (armor > 100) armor = 100;

            // Расчет эффективного здоровья
            double damageReductionFactor = 1.0 / (1.0 - (armor / 100.0));
            return (int)(hp * damageReductionFactor);
        }

        public UnitsProvider(GameModel model_)
        {
            label.AutoSize = false;
            label.Tag = "Level:";
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.Height = levelUpDown.Height;
            UniversalInfoView.TranslateText(label);

            levelUpDown.Maximum = 100;
            levelUpDown.ValueChanged += LevelChanged;

            expLabel.AutoSize = false;
            expLabel.Tag = "ExpToGive:";
            expLabel.TextAlign = ContentAlignment.MiddleCenter;
            expLabel.Height = levelUpDown.Height;
            UniversalInfoView.TranslateText(expLabel);

            expUpDown.Maximum = 1000000;
            expUpDown.Minimum = 0;

            giveExpButton.Tag = "Give exp";
            giveExpButton.Height = levelUpDown.Height;
            giveExpButton.TextOffset = new Point(8, 3);
            giveExpButton.Click += GiveExp;
            giveExpButton.FlatStyle = FlatStyle.Flat;
            giveExpButton.TranslateText();
            model = model_;

            ComboBoxFilter typeFilter = new ComboBoxFilter();
            typeFilter.Init(model, new Size(93, 20), "<Category>",
                (entry, str) => ((Gunit)entry.item).unit_cat.key == (int)str);

            var cats = model.GetAllT<LunitC>();
            foreach (var cat in cats)
            {
                if (cat.text.OrdinalContains("L_NEUTRAL_LEADER"))
                    continue;
                if (cat.text.OrdinalContains("L_NEUTRAL_SOLDIER"))
                    continue;
                typeFilter.AddVariant(new FilterValue(cat.id, cat.text));
            }
            filters.Add(typeFilter);

            ComboBoxFilter levelFilter = new ComboBoxFilter();
            levelFilter.Init(model, new Size(80, 20), "<Level>",
                (entry, str) => ((Gunit)entry.item).level == (int)str);

            for (int i = 1; i < 11; i++)
            {
                levelFilter.AddVariant(new FilterValue(i, "Level " + i.ToString()));
            }
            filters.Add(levelFilter);

            ComboBoxFilter raceFilter = new ComboBoxFilter();
            raceFilter.Init(model, new Size(130, 20), "<Subrace>",
                (entry, str) => ((Gunit)entry.item).subrace.value.id == (int)str);

            var races = model.GetAllT<LSubRace>();
            foreach (var race in races)
            {
                if (race.text.OrdinalContains("L_CUSTOM"))
                    continue;
                raceFilter.AddVariant(new FilterValue(race.id, race.text));
            }
            filters.Add(raceFilter);

            ComboBoxFilter twiceFilter = new ComboBoxFilter();
            twiceFilter.Init(model, new Size(84, 20), "<Attack>",
                (entry, str) => ((Gunit)entry.item).atck_twice == (bool)str);
            twiceFilter.AddVariant(new FilterValue(false, "ATTACK_ONCE"));
            twiceFilter.AddVariant(new FilterValue(true, "ATTACK_TWICE"));
            filters.Add(twiceFilter);

            ComboBoxFilter effectFilter = new ComboBoxFilter();
            ComboBoxFilter resistanceFilter = new ComboBoxFilter();
            ComboBoxFilter immunityFilter = new ComboBoxFilter();

            resistanceFilter.Init(model, new Size(120, 20), "<Ward>",
                (entry, str) => {
                    Gunit unit = (Gunit)entry.item;
                    // Tuple.Item1 - attack type : 0 - effect, 1 - source
                    Tuple<int, int> attackTypeFilter = str as Tuple<int, int>;

                    foreach (Gimmu immu in unit.immun.value)
                    {
                        if (immu.immunecat.value.text == "L_ONCE")
                        {
                            if (attackTypeFilter.Item1 == 1 && immu.immunity.value.id == attackTypeFilter.Item2)
                            {
                                return true;
                            }
                        }
                    }

                    foreach (GimmuC immu in unit.immunCategory.value)
                    {
                        if (immu.immunecat.value.text == "L_ONCE")
                        {
                            if (attackTypeFilter.Item1 == 0 && immu.immunity.value.id == attackTypeFilter.Item2)
                            {
                                return true;
                            }
                        }
                    }
                    return false;
                });

            immunityFilter.Init(model, new Size(120, 20), "<Immunity>",
                (entry, str) => {
                    Gunit unit = (Gunit)entry.item;
                    // Tuple.Item1 - attack type : 0 - effect, 1 - source
                    Tuple<int, int> attackTypeFilter = str as Tuple<int, int>;

                    foreach (Gimmu immu in unit.immun.value)
                    {
                        if (immu.immunecat.value.text == "L_ALWAYS")
                        {
                            if (attackTypeFilter.Item1 == 1 && immu.immunity.value.id == attackTypeFilter.Item2)
                            {
                                return true;
                            }
                        }
                    }

                    foreach (GimmuC immu in unit.immunCategory.value)
                    {
                        if (immu.immunecat.value.text == "L_ALWAYS")
                        {
                            if (attackTypeFilter.Item1 == 0 && immu.immunity.value.id == attackTypeFilter.Item2)
                            {
                                return true;
                            }
                        }
                    }
                    return false;
                });

            effectFilter.Init(model, new Size(120, 20), "<Effect>",
                (entry, str) =>
                {
                    Gunit unit = (Gunit)entry.item;
                    if (unit.attack_id.value != null && unit.attack_id.value.clasS.value.id == (int)str)
                        return true;
                    if (unit.attack2_id.value != null && unit.attack2_id.value.clasS.value.id == (int)str)
                        return true;
                    return false;
                });


            var effects = model.GetAllT<LattC>();
            foreach (var effect in effects)
            {
                effectFilter.AddVariant(new FilterValue(effect.id, effect.text));
                resistanceFilter.AddVariant(new FilterValue(new Tuple<int, int>(0, effect.id), effect.text));
                immunityFilter.AddVariant(new FilterValue(new Tuple<int, int>(0, effect.id), effect.text));
            }
            filters.Add(effectFilter);

            ComboBoxFilter sourceFilter = new ComboBoxFilter();
            sourceFilter.Init(model, new Size(84, 20), "<Source>",
                (entry, str) =>
                {
                    Gunit unit = (Gunit)entry.item;
                    if (unit.attack_id.value != null && unit.attack_id.value.source.value.id == (int)str)
                        return true;
                    if (unit.attack2_id.value != null && unit.attack2_id.value.source.value.id == (int)str)
                        return true;
                    return false;
                }); 
            var sources = model.GetAllT<LattS>();
            foreach (var source in sources)
            {
                if (source.text.OrdinalContains("L_USELESS"))
                    continue;
                if (source.text.StartsWith("L_EMPTY"))
                    continue;
                string attackSource;
                if (source.name_txt.value == null)
                    attackSource = TranslationHelper.Instance().Tr(source.text);
                else
                    attackSource = source.name_txt.value.text;
                sourceFilter.AddVariant(new FilterValue(source.id, attackSource));
                resistanceFilter.AddVariant(new FilterValue(new Tuple<int, int>(1, source.id), attackSource));
                immunityFilter.AddVariant(new FilterValue(new Tuple<int, int>(1, source.id), attackSource));
            }

            filters.Add(sourceFilter);
            filters.Add(resistanceFilter);
            filters.Add(immunityFilter);

            ComboBoxFilter sizeFilter = new ComboBoxFilter();
            sizeFilter.Init(model, new Size(81, 20), "<Size>",
                (entry, str) => ((Gunit)entry.item).size_small == (bool)str);
            
            sizeFilter.AddVariant(new FilterValue(true, "SIZE_SMALL"));
            sizeFilter.AddVariant(new FilterValue(false, "SIZE_BIG"));
            filters.Add(sizeFilter);

            ComboBoxFilter sexFilter = new ComboBoxFilter();
            sexFilter.Init(model, new Size(75, 20), "<Gender>",
                (entry, str) => ((Gunit)entry.item).sex_m == (bool)str);
            sexFilter.AddVariant(new FilterValue(true, "SEX_MALE"));
            sexFilter.AddVariant(new FilterValue(false, "SEX_FEMALE"));
            filters.Add(sexFilter);

            LimitFilter expKillFilter = new LimitFilter();
            expKillFilter.Init(model, new Size(95, 74), "ExpKill",
                (entry, str) => { return true; });
            expKillFilter._getDelegate = (entry) => 
            { 
                return ((Gunit)entry.item).xp_killed; 
            };
            filters.Add(expKillFilter);

            LimitFilter itemCostFilter = new LimitFilter();
            itemCostFilter.Init(model, new Size(95, 74), "Cost",
                (entry, str) => { return true; });
            itemCostFilter._getDelegate = (entry) =>
            {
                int result = CostHelper.G(((Gunit)entry.item).enroll_c);
                return result;
            };
            filters.Add(itemCostFilter);

            model = model_;
            foreach (Gunit unit in model.GetAllT<Gunit>())
            {
                string context = unit.name_txt.value.text;
                if (specialCats.Contains(unit.unit_cat.value.text, StringComparer.OrdinalIgnoreCase))
                    context += " [" + TranslationHelper.Instance().Tr(unit.unit_cat.value.text) + "]";
                allItems.Add(new InfoEntry()
                {
                    context = context,
                    id = unit.unit_id,
                    item = unit
                });
                if (unit.prev_id.value == null)
                    continue;
                if (upgrBinding.ContainsKey(unit.prev_id.key))
                    upgrBinding[unit.prev_id.key].Add(unit.unit_id);
                else
                    upgrBinding.Add(unit.prev_id.key, new List<string>() { unit.unit_id });
            }
            allItems.Sort((x, y) => String.Compare(x.context, y.context));
        }

        string UpgrdToString(int val)
        {
            if (val < 0)
                return val.ToString();
            if (val == 0)
                return "0";
            return "+" + val.ToString();
        }

        int CalcLeveledValue(int value, int startLevel, int resultLevel, int threshold, int up1, int up2)
        {
            int result = value;
            result += (Math.Min(resultLevel, threshold) - startLevel) * up1;
            result += (Math.Max(resultLevel, threshold) - threshold) * up2;
            return result;
        }

        int СalcConsumedExp(string unit, int level, bool withCurrent = false)
        {
            Gunit gunit = model.GetObjectT<Gunit>(unit);
            GDynUpgr upgr1 = gunit.dyn_upg1.value;
            GDynUpgr upgr2 = gunit.dyn_upg2.value;
            int result = 0;
            if (withCurrent)
                result += gunit.xp_next;
            for(int i = gunit.level; i < level; ++i)
            {
                result += CalcLeveledValue(gunit.xp_next, gunit.level, i, gunit.dyn_upg_lv, upgr1.xp_next, upgr2.xp_next);
            }
            if (gunit.prev_id.value != null)
                result += СalcConsumedExp(gunit.prev_id.value.unit_id, gunit.prev_id.value.level, true);
            return result;
        }

        int DrawCostValues(Graphics g, int x, int y, string name, Image resource,
            int val1, int val2, int val3,
            int startLevel, int resultLevel, int threshold, int up1, int up2)
        {
            if (val1 > 0 || val2 > 0 || val3 > 0)
            {
                g.DrawImage(resource, new Rectangle(x, y, rowH, rowH));
                ImageHelper.DrawTableCell(g, x + rowH, y, "",
                    CalcLeveledValue(val1, startLevel, resultLevel, threshold, up1, up2).ToString(),
                    UpgrdToString(val2), UpgrdToString(val3), rowH, 120, nameFont, baseFont);
                return rowH;
            }
            return 0;
        }

        int DrawCostCells(Graphics g, int x, int y, string name, string cost1, string cost2, string cost3,
            int startLevel, int resultLevel, int threshold)
        {
            int w = nameW;
            if (name == "")
                w = 0;
            else
                g.DrawString(TranslationHelper.Instance().Tr(name), nameFont, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(x, y, nameW, rowH));
            int G1 = CostHelper.G(cost1);
            int R1 = CostHelper.R(cost1);
            int Y1 = CostHelper.Y(cost1);
            int E1 = CostHelper.E(cost1);
            int W1 = CostHelper.W(cost1);
            int B1 = CostHelper.B(cost1);

            int G2 = CostHelper.G(cost2);
            int R2 = CostHelper.R(cost2);
            int Y2 = CostHelper.Y(cost2);
            int E2 = CostHelper.E(cost2);
            int W2 = CostHelper.W(cost2);
            int B2 = CostHelper.B(cost2);

            int G3 = CostHelper.G(cost3);
            int R3 = CostHelper.R(cost3);
            int Y3 = CostHelper.Y(cost3);
            int E3 = CostHelper.E(cost3);
            int W3 = CostHelper.W(cost3);
            int B3 = CostHelper.B(cost3);

            int newX = x + w;
            int newY = y;
            newY += DrawCostValues(g, newX, newY, "", GetImageById("IntfScen", "_RESOURCES_GOLD"), G1, G2, G3,
                startLevel, resultLevel, threshold, G2, G3);
            newY += DrawCostValues(g, newX, newY, "", GetImageById("IntfScen", "_RESOURCES_REDM_B"), R1, R2, R3,
                startLevel, resultLevel, threshold, R2, R3);
            newY += DrawCostValues(g, newX, newY, "", GetImageById("IntfScen", "_RESOURCES_BLUEM_B"), Y1, Y2, Y3,
                startLevel, resultLevel, threshold, Y2, Y3);
            newY += DrawCostValues(g, newX, newY, "", GetImageById("IntfScen", "_RESOURCES_BLACKM_B"), E1, E2, E3,
                startLevel, resultLevel, threshold, E2, E3);
            newY += DrawCostValues(g, newX, newY, "", GetImageById("IntfScen", "_RESOURCES_WHITEM_B"), W1, W2, W3,
                startLevel, resultLevel, threshold, W2, W3);
            newY += DrawCostValues(g, newX, newY, "", GetImageById("IntfScen", "_RESOURCES_GREENM_B"), B1, B2, B3,
                startLevel, resultLevel, threshold, B2, B3);
            if (cost2 == "" || cost2 == null)
                objectDesc.Add(TranslationHelper.Instance().Tr(name) + " " + cost1);
            else
                objectDesc.Add(TranslationHelper.Instance().Tr(name) + 
                    " " + cost1 + "(" + cost2 + "[" + startLevel.ToString() + "] : " + cost3 + ")");
            return newY;
        }

        public string UpgrdDamageToString(Gattack skill, Gattack skill2, GDynUpgr upgr1)
        {
            if (skill.clasS.value.text == "L_HEAL" ||
                    (skill2 != null && skill2.clasS.value.text == "L_HEAL") ||
                    (skill.clasS.value.text == "L_BESTOW_WARDS" && upgr1.heal > 0) ||
                    (skill2 != null && skill2.clasS.value.text == "L_BESTOW_WARDS" && upgr1.heal > 0))
            {
                return UpgrdToString(upgr1.heal);
            }
            return UpgrdToString(upgr1.damage);
        }

        public string AttackDamageString(Gattack skill, int value)
        {
            string result = value.ToString();
            if (skill.reach.value.reach_txt.value != null)
            {
                if (skill.dam_split)
                    result += TranslationHelper.Tr_(", split between targets");
                else
                {
                    float newVal = value * skill.dam_ratio / 100.0f;
                    LAttR range = skill.reach.value;
                    if (skill.dr_repeat)
                    {
                        if (skill.dam_ratio != 100)
                        {
                            for (int i = 1; i < range.max_targts; ++i)
                            {
                                result += ", " + ((int)(Math.Round(newVal))).ToString();
                                newVal = newVal * skill.dam_ratio / 100.0f;
                            }
                        }
                    }
                    else
                    {
                        if (skill.dam_ratio != 100 && skill.dam_ratio != 0)
                        {
                            result += ", ";
                            if (range.max_targts > 2)
                                result += "(x" + (range.max_targts - 1).ToString() + ") ";
                            result += ((int)(Math.Round(newVal))).ToString();
                        }
                    }
                }
            }
            return result;
        }

        public void GetAttackDamageString(Gattack skill, ref string skillName, ref string name, ref string value, ref string accuracy,
            Gunit unit, int level)
        {
            GVars vars = model.GetAllT<GVars>()[0];
            GDynUpgr upgr1 = unit.dyn_upg1.value;
            GDynUpgr upgr2 = unit.dyn_upg2.value;
            string extra = "";
            if (skill.clasS.value.text == "L_TRANSFORM_SELF" && skill.alt_attack.value != null)
            {
                Gattack altAttack = skill.alt_attack.value;
                accuracy = CalcLeveledValue(altAttack.power, unit.level, level, unit.dyn_upg_lv, upgr1.power, upgr2.power).ToString() + "%";
            }
            else
                accuracy = CalcLeveledValue(skill.power, unit.level, level, unit.dyn_upg_lv, upgr1.power, upgr2.power).ToString() + "%";
            if (skill.alt_attack.value != null)
            {
                Gattack altAttack1 = skill.alt_attack.value;
                extra += " " + TranslationHelper.Instance().Tr("or") + " " + altAttack1.name_txt.value.text;
                string tmpName = "";
                GetAttackDamageString(altAttack1, ref tmpName, ref name, ref value, ref accuracy, unit, level);
                skillName = skill.name_txt.value.text + extra;
                return;
            }
            if (skill.clasS.value.text == "L_BOOST_DAMAGE")
            {
                name = "Boost:";
                switch(skill.level)
                {
                    case 1:
                        value = (vars.batboostd1).ToString() + "%";
                        break;
                    case 2:
                        value = (vars.batboostd2).ToString() + "%";
                        break;
                    case 3:
                        value = (vars.batboostd3).ToString() + "%";
                        break;
                    case 4:
                        value = (vars.batboostd4).ToString() + "%";
                        break;
                }
                skillName = skill.name_txt.value.text + extra;
                return;
            }
            if (skill.clasS.value.text == "L_BESTOW_WARDS")
            {
                if (skill.qty_heal > 0 || upgr1.heal > 0 || upgr2.heal > 0)
                {
                    name = "Heal:";
                    int val = CalcLeveledValue(skill.qty_heal, unit.level, level, unit.dyn_upg_lv, upgr1.heal, upgr2.heal);
                    value = AttackDamageString(skill, val);
                }
                else
                {
                    name = "";
                    value = "";
                }
                skillName = skill.name_txt.value.text + extra;
                return;
            }
            if (skill.clasS.value.text == "L_LOWER_INITIATIVE")
            {
                name = "";
                switch (skill.level)
                {
                    case 1:
                        value = (vars.batloweri1).ToString() + "%";
                        break;
                }
                skillName = skill.name_txt.value.text + extra;
                return;
            }

            if (skill.clasS.value.text == "L_LOWER_DAMAGE")
            {
                name = "";
                switch (skill.level)
                {
                    case 1:
                        value = (vars.batlowerd1).ToString() + "%";
                        break;
                    case 2:
                        value = (vars.batlowerd2).ToString() + "%";
                        break;
                }
                skillName = skill.name_txt.value.text + extra;
                return;
            }
            if (skill.clasS.value.text == "L_HEAL")
            {
                name = "Heal:";
                int val = CalcLeveledValue(skill.qty_heal, unit.level, level, unit.dyn_upg_lv, upgr1.heal, upgr2.heal);
                value = AttackDamageString(skill, val);
                skillName = skill.name_txt.value.text + extra;
                return;
            }
            if (skill.clasS.value.text == "L_PARALYZE" || skill.clasS.value.text == "L_PETRIFY")
            {
                //name = "Disable";
                if (skill.infinite)
                    skillName = skill.name_txt.value.text + extra + " " + TranslationHelper.Instance().Tr("(Lasting)");
                else
                    skillName = skill.name_txt.value.text + extra;
                return;
            }
            if (skill.clasS.value.text.IsIn("L_DAMAGE", "L_DRAIN", "L_DRAIN_OVERFLOW", "L_POISON", "L_FROSTBITE", "L_BLISTER", "L_SHATTER"))
            {
                name = "AttackDamage:";
                int val = CalcLeveledValue(skill.qty_dam, unit.level, level, unit.dyn_upg_lv, upgr1.damage, upgr2.damage);
                value = AttackDamageString(skill, val);
                skillName = skill.name_txt.value.text + extra;
                if (skill.infinite)
                    skillName += " " + TranslationHelper.Instance().Tr("(Lasting)");


                if (skill.crit_hit || skill.crit_power > 0)
                {
                    int critMult = 5;
                    int critChance = 100;
                    if (model._luaSettings.HasLuaSettings())
                    {
                         if (!int.TryParse(model._luaSettings.getProperty("criticalHitChance"), out critChance))
                            critChance = 100;
                        if (!int.TryParse(model._luaSettings.getProperty("criticalHitDamage"), out critMult))
                            critMult = 5;
                    }
                    if (skill.crit_power > 0 || skill.crit_dam > 0)
                    {
                        critMult = skill.crit_dam;
                        if (skill.crit_power > 0)
                            critChance = skill.crit_power;
                    }
                    skillName += " [ " + TranslationHelper.Instance().Tr("crit") + " " + critMult + "%]";
                    int critVal = (val * critMult) / 100;
                    value += " [ " + critVal.ToString() + " ]";
                    //if (critChance != 100)
                        accuracy += "[ " + critChance.ToString() + "%]";
                }
                return;
            }

            name = "";
            value = "";
            skillName = skill.name_txt.value.text + extra;
        }

        void DrawTableCell(Graphics g, int x, int y, string name, string value, int rowCount = 1, int h = 18)
        {
            int w = string.IsNullOrEmpty(name) ? 0 : nameW;
            name = TranslationHelper.Instance().Tr(name);
            var brush = new SolidBrush(StyleService.GetColor("imageFont"));
            g.DrawString(name, nameFont, brush, new RectangleF(x, y, nameW, h * rowCount));
            g.DrawString(value, baseFont, brush, new RectangleF(x + w, y, 170, h * rowCount));
            objectDesc.Add(name + " - " + value);
        }
        void DrawTableCell(Graphics g, int x, int y, string name, string value1, string value2, string value3, int rowCount = 1, int h = 18)
        {
            int w = string.IsNullOrEmpty(name) ? 0 : nameW;
            name = TranslationHelper.Instance().Tr(name);
            var brush = new SolidBrush(StyleService.GetColor("imageFont"));
            g.DrawString(name, nameFont, brush, new RectangleF(x, y, 170, h * rowCount));
            g.DrawString(value1, baseFont, brush, new RectangleF(x + w, y, 170, h * rowCount));
            string extra = MergedUpgrdDamage(value2, value3);
            float value1Width = g.MeasureString(value1, baseFont).Width;
            g.DrawString(extra, extraFont, brush, new RectangleF(x + w + value1Width, y, 170, h * rowCount));
            objectDesc.Add(name + " - " + value1 + extra);
        }

        string MergedUpgrdDamage(string value2, string value3)
        {
            string extra = "";
            if (value2 == "0" && value3 == "0")
                extra = "";
            else
            {
                if (value2 == value3)
                    extra = "(" + value2 + ")";
                else
                {
                    extra = "(" + value2 + " / " + value3 + ")";
                }
            }
            return extra;
        }
        void DrawTableCell(Graphics g, int x, int y, string name, string value1, string value2)
        {
            int w = string.IsNullOrEmpty(name) ? 0 : nameW;
            name = TranslationHelper.Instance().Tr(name);
            var brush = new SolidBrush(StyleService.GetColor("imageFont"));
            g.DrawString(name, nameFont, brush, new RectangleF(x, y, 140, 30));
            g.DrawString(value1, baseFont, brush, new RectangleF(x + w, y, 170, rowH));
            string extra = string.IsNullOrEmpty(value2) ? "" : "(" + value2 + ")";
            float value1Width = g.MeasureString(value1, baseFont).Width;
            g.DrawString(extra, extraFont, brush, new RectangleF(x + w + value1Width, y, 170, rowH));
            objectDesc.Add(name + " - " + value1 + extra);
        }

        void DrawTableCell2(Graphics g, int x, ref int y, int w, string name, string value)
        {
            int wRes = string.IsNullOrEmpty(name) ? 0 : nameW;
            name = TranslationHelper.Instance().Tr(name);
            var brush = new SolidBrush(StyleService.GetColor("imageFont"));
            g.DrawString(name, nameFont, brush, new RectangleF(x, y, 140, 30));
            int height = Math.Max(rowH, (int)g.MeasureString(value, extraFont, w).Height + 2);
            value = value.Replace("\r", "").Replace("\\n", "\n");
            g.DrawString(value, extraFont, brush, new RectangleF(x + wRes, y, w, height));
            //g.DrawRectangle(new Pen(Color.Red), new Rectangle(x + wRes, y, w, height));
            y += height;
            objectDesc.Add(name + " - " + value);
        }

        void DrawModifier(Graphics g, Gmodif mod, ref ViewItem result, ref int modX, int posY)
        {
            if (mod != null && mod.display)
            {
                Rectangle modRect = new Rectangle(modX, posY, 30, 30);
                string text = "";
                if (mod.desc_txt.value != null)
                    text = RemoveStyles(mod.desc_txt.value.text);
                if (HasResource("Icons", mod.modif_id.ToUpper()))
                {
                    Image modifImage = GetImageById("Icons", mod.modif_id.ToUpper());
                    g.DrawImage(modifImage, modRect);
                }
                g.DrawRectangle(new Pen(new SolidBrush(StyleService.GetColor("imageFont")), 1), modRect);

                result.clickables.Add(new ClickableArea()
                {
                    id = mod.modif_id,
                    pos = modRect,
                    tooltip = text
                });
                modX += 35;
                objectDesc.Add("modificator: " + text);
            }
        }

        void DrawAbil(Graphics g, LleadA mod, ref ViewItem result, ref int modX, int posY)
        {
            if (mod != null)
            {
                Rectangle modRect = new Rectangle(modX, posY, 30, 30);
                string text = "" + mod.text;//x000tgaNNN
                string image = "ABIL";
                if (mod.text == "L_ZEALOT")
                {
                    image += "";
                    text = "";
                }
                else if (mod.text == "L_INCORRUPTIBLE")
                {
                    image += "0007";
                    text = "X005ta0175";
                }
                else if (mod.text == "L_WEAPON_MASTER")
                {
                    image += "0008";
                    text = "X005ta0180";
                }
                else if (mod.text == "L_WAND_SCROLL_USE")
                {
                    image += "0001";
                    text = "X005ta0181";
                }
                else if (mod.text == "L_WEAPON_ARMOR_USE")
                {
                    image += "0002";
                    text = "X005ta0182";
                }
                else if (mod.text == "L_BANNER_USE")
                {
                    image += "0003";
                    text = "X005ta0183";
                }
                else if (mod.text == "L_JEWELRY_USE")
                {
                    image += "0006";
                    text = "X005ta0184";
                }
                else if (mod.text == "L_LUCK_STACK")
                {
                    image += "";
                    text = "";
                }
                else if (mod.text == "L_ROD")
                {
                    image += "0004";
                    text = "X005ta0359";
                }
                else if (mod.text == "L_ORB_USE")
                {
                    image += "0009";
                    text = "X005ta0176";
                }
                else if (mod.text == "L_TALISMAN_USE")
                {
                    image += "0010";
                    text = "X005ta0177";
                }
                else if (mod.text == "L_TRAVEL_ITEM_USE")
                {
                    image += "0011";
                    text = "X005ta0178";
                }
                else if (mod.text == "L_CRITICAL_HIT")
                {
                    image += "0020";
                    text = "X160ta0017";
                }

                if (HasResource("Icons", image))
                {
                    Image modifImage = GetImageById("Icons", image);
                    g.DrawImage(modifImage, modRect);
                }
                g.DrawRectangle(new Pen(new SolidBrush(StyleService.GetColor("imageFont")), 1), modRect);

                string resText = model.GetObjectT<TApp>(text).text;


                result.clickables.Add(new ClickableArea()
                {
                    id = mod.id.ToString(),
                    pos = modRect,
                    tooltip = resText
                });
                modX += 35;
                objectDesc.Add("modificator: " + text);
            }
        }

        private ViewItem ProcessUnit(Gunit unit, int level, bool withHelp = true, bool editorMode = false)
        {
            ViewItem result = new ViewItem();
            objectDesc.Clear();
            Image image = StyleService.GetImage("back");
            offsetX = StyleService.GetCustomInt("offsetX");
            offsetY = StyleService.GetCustomInt("offsetY");
            if (image == null)
            {
                image = new Bitmap(520 + offsetX * 2, 520 + offsetY * 2);
                using (Graphics gfx = Graphics.FromImage(image))
                using (SolidBrush brush = new SolidBrush(StyleService.GetColor("back")))
                {
                    gfx.FillRectangle(brush, 0, 0, image.Width, image.Height);
                }
            }
            else
                image = GameResourceModel.ResizeImage(image, new Size(800, 680));
 
            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;
            string unitIconId = unit.base_unit.key + "FACE";
            if (unit.base_unit.value == null)
                unitIconId = unit.unit_id + "FACE";
            //if (!HasResource("Faces", unitIconId.ToUpper()))
            //    unitIconId = unit.baseUnit +"FACE";
            GDynUpgr upgr1 = unit.dyn_upg1.value;
            GDynUpgr upgr2 = unit.dyn_upg2.value;
            Image unitIm = GetImageById("Faces", unitIconId.ToUpper());
            unitIm = GameResourceModel.ResizeImage(unitIm, new Size((int)(unitIm.Width * 1.35), (int)(unitIm.Height * 1.35)));
            using (Graphics g = Graphics.FromImage(image))
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                int posY = offsetY;
                int firstPartW = image.Width / 2 - offsetX - 40;
                int secondPartW = image.Width - firstPartW - offsetX * 2;
                int posX = offsetX + (firstPartW - unitIm.Width) / 2 - 10;
                var brush = new SolidBrush(StyleService.GetColor("imageFont"));
                g.DrawString(unit.name_txt.value.text, unitNameFont, brush,
                    new RectangleF(offsetX - 10, posY + unitIm.Height, firstPartW, 30), sf);
                g.DrawString(unit.desc_txt.value.text.Replace("\\n", " "), baseFont, brush,
                    new RectangleF(offsetX, posY + unitIm.Height + 34, firstPartW, 100));

                g.DrawImage(unitIm, new Rectangle(posX, posY, unitIm.Width, unitIm.Height));
                if (unit.size_small && HasResource("Icons", "BORDERUNITSMALL"))
                {
                    Image borderImage = GetImageById("Icons", "BORDERUNITSMALL");
                    g.DrawImage(borderImage, new Rectangle(posX - 5, posY - 5, unitIm.Width + 10, unitIm.Height + 10));
                }
                if (!unit.size_small && HasResource("Icons", "BORDERUNITLARGE"))
                {
                    Image borderImage = GetImageById("Icons", "BORDERUNITLARGE");
                    g.DrawImage(borderImage, new Rectangle(posX - 5, posY - 5, unitIm.Width + 10, unitIm.Height + 10));
                }
                posY = posY + unitIm.Height + 34 + 100;
                if (unit.abils.Count() > 0)
                {
                    int modX = offsetX - 5;
                    for(int i = 0;i < unit.abils.Count();++i)
                        DrawAbil(g, unit.abils.value[i].l_ability.value, ref result, ref modX, posY);

                    posY += 35;
                }
                if (unit.modifs.Count() > 0)
                {
                    List<Gmodif> modifs = UnitModifiersList(unit);
                    int modX = offsetX - 5;
                    foreach(var mod in modifs) 
                        DrawModifier(g, mod, ref result, ref modX, posY);

                    posY += 35;
                }
                if (unit.enroll_b.value != null && unit.unit_cat.value.text != "L_LEADER")
                {
                    Image buildIm = GetImageById("IconBld", unit.enroll_b.key.ToUpper());
                    int buildX = offsetX;
                    g.DrawString(TranslationHelper.Instance().Tr("Living in:"), baseFont, brush,
                        new RectangleF(buildX, posY, 100, rowH));

                    Rectangle buildRect = new Rectangle(buildX, posY + rowH, buildIm.Width, buildIm.Height);
                    g.DrawImage(buildIm, buildRect);

                    result.clickables.Add(new ClickableArea()
                    {
                        id = unit.enroll_b.value.build_id,
                        pos = buildRect,
                        tooltip = unit.enroll_b.value.name_txt.value.text
                    });
                    objectDesc.Add(TranslationHelper.Instance().Tr("Living in: ") + unit.enroll_b.value.name_txt.value.text);
                    posY = DrawCostCells(g, buildX + 10, posY + buildIm.Height + rowH, "", unit.enroll_b.value.cost, "", "", unit.level, level, unit.dyn_upg_lv);
                    //posY = posY + buildIm.Height + rowH * 2 + rowH / 2;
                }
                int oldNameW = nameW;
                nameW -= 24;
                posY = DrawCostCells(g, offsetX - 10, posY, "Cost:", unit.enroll_c, upgr1.enroll_c,
                    upgr2.enroll_c, unit.level, level, unit.dyn_upg_lv);
                posY = DrawCostCells(g, offsetX - 10, posY, "ReviveCost:", unit.revive_c, upgr1.revive_c,
                    upgr2.revive_c, unit.level, level, unit.dyn_upg_lv);
                posY = DrawCostCells(g, offsetX - 10, posY, "HealCost:", unit.heal_c, upgr1.heal_c,
                    upgr2.heal_c, unit.level, level, unit.dyn_upg_lv);
                nameW = oldNameW;

                if (unit.upgrade_b.value != null && unit.unit_cat.value.text != "L_LEADER")
                {
                    posY += 35;
                    Image buildIm = GetImageById("IconBld", unit.upgrade_b.key.ToUpper());
                    int buildX = offsetX;
                    g.DrawString(TranslationHelper.Instance().Tr("Living in:"), baseFont, brush,
                        new RectangleF(buildX, posY, 100, rowH));
                    Rectangle buildRect = new Rectangle(buildX, posY + rowH, buildIm.Width, buildIm.Height);
                    g.DrawImage(buildIm, buildRect);
                    Gbuild building = model.GetObjectT<Gbuild>(unit.upgrade_b.key);

                    result.clickables.Add(new ClickableArea()
                    {
                        id = building.build_id,
                        pos = buildRect,
                        tooltip = building.name_txt.value.text
                    });
                    int oldY = posY;
                    objectDesc.Add(TranslationHelper.Instance().Tr("Living in: ") + building.name_txt.value.text);
                    posY = DrawCostCells(g, buildX + 10, posY + buildIm.Height + rowH + 5, "", building.cost, "", "", unit.level, level, unit.dyn_upg_lv);

                    if (unit.prev_id.value != null)
                    {
                        string prevIconId = unit.prev_id.value.base_unit.key + "FACEB";
                        if (unit.prev_id.value.base_unit.value == null)
                            prevIconId = unit.prev_id.key + "FACEB";
                        prevIconId = prevIconId.ToUpper();
                        if (HasResource("Faces", prevIconId))
                        {
                            Image prevIm = GetImageById("Faces", prevIconId);
                            int prevX = buildX + buildIm.Width + 25;
                            g.DrawString(TranslationHelper.Instance().Tr("Trained from:"), baseFont, brush,
                                new RectangleF(prevX, oldY, 100, rowH));
                            g.DrawImage(prevIm, new Rectangle(prevX + 10, oldY + rowH, buildIm.Width, buildIm.Height));

                            result.clickables.Add(new ClickableArea()
                            {
                                id = unit.prev_id.key,
                                pos = new Rectangle(prevX + 10, oldY + rowH, buildIm.Width, buildIm.Height),
                                tooltip = unit.prev_id.value.name_txt.value.text
                            });
                            objectDesc.Add(TranslationHelper.Instance().Tr("Trained from:") + 
                                unit.prev_id.value.name_txt.value.text +
                                "[" + unit.prev_id.key + "]");

                        }
                    }
                    //posY = oldY + buildIm.Height + rowH * 2 + rowH / 2;
                }

                
                if (specialCats.Contains(unit.unit_cat.value.text))
                {
                    List<Gaction> actions = model.GetAllT<Gaction>();
                    Rectangle typeRect = new Rectangle(offsetX, posY += rowH * 2, firstPartW, rowH);
                    g.DrawString(TranslationHelper.Instance().Tr(unit.unit_cat.value.text), unitCatFont, brush, typeRect, sf);
                    if (unit.unit_cat.value.text == "L_LEADER" || unit.unit_cat.value.text == "L_NOBLE")
                    {
                        DrawTableCell(g, offsetX, posY += rowH * 2, "Leadership:", unit.leadership.ToString());
                        if (unit.unit_cat.value.text == "L_NOBLE")
                        {
                            typeRect = new Rectangle(offsetX + nameW + 25, posY - rowH * 2, 16, 16);
                            string actionsLTooltip = TranslationHelper.Instance().Tr("nobble_leaders") + "\n";
                            string actionsVTooltip = TranslationHelper.Instance().Tr("nobble_village") + "\n";
                            string actionsLVTooltip = TranslationHelper.Instance().Tr("nobble_village_and_leader") + "\n";
                            string actionsTTooltip = TranslationHelper.Instance().Tr("nobble_traders") + "\n";
                            string actionsOTooltip = TranslationHelper.Instance().Tr("nobble_other") + "\n";
                            foreach (Gaction action in actions)
                            {
                                int val = action.chance;
                                if (new List<int>() { 0, 2, 5 }.Contains(action.category))
                                {
                                    actionsLTooltip += action.name_txt.value.text + " - " + val.ToString() + "%\n";
                                    continue;
                                }
                                if (new List<int>() { 6, 7, 8, 9, 13 }.Contains(action.category))
                                {
                                    if (action.category == 8 || action.category == 13)
                                        actionsVTooltip += action.name_txt.value.text + " - " + val.ToString() + "%\n";
                                    else
                                        actionsVTooltip += action.name_txt.value.text + "* - " + val.ToString() + "%\n";
                                    continue;
                                }
                                if (new List<int>() { 1, 3, 4 }.Contains(action.category))
                                {
                                    if (action.category == 1)
                                        actionsLVTooltip += action.name_txt.value.text + " - " + val.ToString() + "%\n";
                                    else
                                        actionsLVTooltip += action.name_txt.value.text + "* - " + val.ToString() + "%\n";
                                    continue;
                                }
                                if (new List<int>() { 10, 11 }.Contains(action.category))
                                {
                                    actionsTTooltip += action.name_txt.value.text + " - " + val.ToString() + "%\n";
                                    continue;
                                }

                                actionsOTooltip += action.name_txt.value.text + " - " + val.ToString() + "%\n";
                            }
                            string actionsTooltip = actionsLTooltip + "\n";
                            actionsTooltip += actionsVTooltip + "\n";
                            actionsTooltip += actionsLVTooltip + "\n";
                            actionsTooltip += actionsOTooltip + "\n";
                            actionsTooltip += "\n" + TranslationHelper.Instance().Tr("nobble_village_info");

                            result.clickables.Add(new ClickableArea()
                            {
                                id = null,
                                pos = typeRect,
                                tooltip = actionsTooltip
                            });
                            g.DrawEllipse(new Pen(StyleService.GetColor("imageFont")), typeRect);
                            typeRect.Y += 2;
                            g.DrawString("?", new Font("comic sans", 8, FontStyle.Bold), brush, typeRect, sf);
                        }
                    }
                    else
                    {
                        DrawTableCell(g, offsetX, posY += rowH * 2, "Life time:", unit.life_time.ToString());
                    }
                    DrawTableCell(g, offsetX, posY += rowH, "Move:",
                            CalcLeveledValue(unit.move, unit.level, level, unit.dyn_upg_lv, upgr1.move, upgr2.move).ToString(),
                            UpgrdToString(upgr1.move), UpgrdToString(upgr2.move));
                    DrawTableCell(g, offsetX, posY += rowH, "Scout:", unit.scout.ToString());
                    
                    int nobilityValue = CalcLeveledValue(unit.negotiate + 10, unit.level, level, unit.dyn_upg_lv, upgr1.negotiate, upgr2.negotiate);
                    Rectangle nobilityRect = new Rectangle(offsetX + nameW + 70, posY + rowH, 16,16);
                    DrawTableCell(g, offsetX, posY += rowH, "Nobility:",
                        nobilityValue.ToString() + "%",
                        UpgrdToString(upgr1.negotiate), UpgrdToString(upgr2.negotiate));
                    string nobilityTooltip = "";
                    foreach (Gaction action in actions)
                    {
                        if (action.category == 1)//send spy
                            continue;
                        if (action.category < 5)//actions on stack
                        {
                            int val = action.chance - nobilityValue + 10;
                            nobilityTooltip += action.name_txt.value.text + " - " + val.ToString() + "%\n";
                        }
                    }
                    g.DrawEllipse(new Pen(StyleService.GetColor("imageFont")), nobilityRect);

                    result.clickables.Add(new ClickableArea()
                    {
                        id = null,
                        pos = nobilityRect,
                        tooltip = nobilityTooltip
                    });
                    nobilityRect.Y += 2;
                    g.DrawString("?", new Font("comic sans", 8, FontStyle.Bold), brush, nobilityRect, sf);
                }

                posX = offsetX + firstPartW;
                posY = offsetY;

                if (editorMode)
                    DrawTableCell(g, posX, posY += rowH, "ExpConsumed:", СalcConsumedExp(unit.unit_id, level).ToString());
                DrawTableCell(g, posX, posY += rowH, "Level / threshold:", level.ToString(), unit.dyn_upg_lv.ToString());
                DrawTableCell(g, posX, posY += rowH, "Exp:",
                    CalcLeveledValue(unit.xp_next, unit.level, level, unit.dyn_upg_lv, upgr1.xp_next, upgr2.xp_next).ToString(),
                    UpgrdToString(upgr1.xp_next), UpgrdToString(upgr2.xp_next));
                DrawTableCell(g, posX, posY += rowH, "ExpKill:",
                    CalcLeveledValue(unit.xp_killed, unit.level, level, unit.dyn_upg_lv, upgr1.xp_killed, upgr2.xp_killed).ToString(),
                    UpgrdToString(upgr1.xp_killed), UpgrdToString(upgr2.xp_killed));
                

                posY += (int)(rowH * 1.5);
                int hp = CalcLeveledValue(unit.hit_point, unit.level, level, unit.dyn_upg_lv, upgr1.hit_point, upgr2.hit_point);
                int armor = CalcLeveledValue(unit.armor, unit.level, level, unit.dyn_upg_lv, upgr1.armor, upgr2.armor);
                DrawTableCell(g, posX, posY += rowH, "HP:",
                    hp.ToString(),
                    UpgrdToString(upgr1.hit_point), UpgrdToString(upgr2.hit_point));
                g.DrawString("[ " + calculateEffectiveHp(hp, armor).ToString() + " ]", baseFont, brush, 
                    new Rectangle(posX + 230, posY, 80, rowH), sf);
                
                DrawTableCell(g, posX, posY += rowH, "Regen, %:",
                    CalcLeveledValue(unit.regen, unit.level, level, unit.dyn_upg_lv, upgr1.regen, upgr2.regen).ToString(),
                    UpgrdToString(upgr1.regen), UpgrdToString(upgr2.regen));
                DrawTableCell(g, posX, posY += rowH, "Armor:",
                    armor.ToString(),
                    UpgrdToString(upgr1.armor), UpgrdToString(upgr2.armor));

                int immunW = secondPartW - nameW;
                string immunities = "";
                string resistances = "";
                foreach (Gimmu immu in unit.immun.value)
                {
                    if (immu.immunecat.value.text == "L_ONCE")
                    {
                        resistances += ((resistances == "") ? "" : ", ");
                        if (immu.immunity.value.name_txt.value == null)
                            resistances += TranslationHelper.Instance().Tr(immu.immunity.value.text);
                        else
                            resistances += immu.immunity.value.name_txt.value.text;
                    }
                    else if (immu.immunecat.value.text == "L_ALWAYS")
                    {
                        immunities += ((immunities == "") ? "" : ", ");
                        if (immu.immunity.value.name_txt.value == null)
                            immunities += TranslationHelper.Instance().Tr(immu.immunity.value.text);
                        else
                            immunities += immu.immunity.value.name_txt.value.text;
                    }
                }
                foreach (GimmuC immu in unit.immunCategory.value)
                {
                    if (immu.immunecat.value.text == "L_ONCE")
                    {
                        resistances += ((resistances == "") ? "" : ", ");
                        resistances += TranslationHelper.Instance().Tr(immu.immunity.value.text);
                    }
                    else if (immu.immunecat.value.text == "L_ALWAYS")
                    {
                        immunities += ((immunities == "") ? "" : ", ");
                        immunities += TranslationHelper.Instance().Tr(immu.immunity.value.text);
                    }
                }

                if (immunities == "")
                    immunities = TranslationHelper.Instance().Tr("None");
                if (resistances == "")
                    resistances = TranslationHelper.Instance().Tr("None");
                posY += rowH;
                DrawTableCell2(g, posX, ref posY, secondPartW - nameW, TranslationHelper.Instance().Tr("Immunity:"), immunities);
                DrawTableCell2(g, posX, ref posY, secondPartW - nameW, TranslationHelper.Instance().Tr("Resistance:"), resistances);

                Gattack attack1 = unit.attack_id.value;
                Gattack attack2 = unit.attack2_id.value;

                string powerName1 = "";
                string powerName2 = "";
                string attackText = "";
                string attackPower = "";
                string attackAcc = "";


                GetAttackDamageString(attack1, ref attackText, ref powerName1, ref attackPower, ref attackAcc, unit, level);
                if (unit.atck_twice)
                    attackText = "(2x) " + attackText;

                string attackSource = "";
                if (attack1.alt_attack.value != null && attack1.clasS.value.text == "L_TRANSFORM_SELF")
                {
                    if (attack1.alt_attack.value.source.value.name_txt.value == null)
                        attackSource = TranslationHelper.Instance().Tr(attack1.alt_attack.value.source.value.text);
                    else
                        attackSource = attack1.alt_attack.value.source.value.name_txt.value.text;
                }
                if (attackSource == "")
                {
                    if (attack1.source.value.name_txt.value == null)
                        attackSource = TranslationHelper.Instance().Tr(attack1.source.value.text);
                    else
                        attackSource = attack1.source.value.name_txt.value.text;
                }
                
                if (attack2 != null)
                {
                    string attackText2 = "";
                    string attackPower2 = attack2.qty_dam.ToString();
                    string tmp="";
                    GetAttackDamageString(attack2, ref attackText2, ref powerName2, ref attackPower2, ref tmp, unit, level);
                    if (attackPower2 != "")
                    {
                        if (attackPower == "")
                            attackPower = attackPower2;
                        else
                            attackPower += " / " + attackPower2;
                    }

                    attackText += " / " + attackText2;

                    if (attack2.source.value.name_txt.value == null)
                        attackSource += " / " + TranslationHelper.Instance().Tr(attack2.source.value.text);
                    else
                        attackSource += " / " + attack2.source.value.name_txt.value.text;

                    if (powerName1 == "")
                        powerName1 = powerName2;
                    if (attack2.power != 0)
                        attackAcc += " / " + CalcLeveledValue(attack2.power, unit.level, level, unit.dyn_upg_lv, upgr1.power, upgr2.power).ToString() + "%";
                }
                posY += rowH;
                DrawTableCell2(g, posX, ref posY, secondPartW - nameW, TranslationHelper.Instance().Tr("Attack:"), attackText);
                //posY += rowH;
                DrawSkillTransf(g, attack1, posX, secondPartW, image.Width, ref posY, ref result, offsetX);
                DrawSkillTransf(g, attack1.alt_attack.value, posX, secondPartW, image.Width, ref posY, ref result, offsetX);
                if (attack2 != null)
                {
                    DrawSkillTransf(g, attack2, posX, secondPartW, image.Width, ref posY, ref result, offsetX);
                    DrawSkillTransf(g, attack2.alt_attack.value, posX, secondPartW, image.Width, ref posY, ref result, offsetX);
                }

                DrawTableCell(g, posX, posY, "Source:", attackSource);
                DrawTableCell(g, posX, posY += rowH, "Accuracy:", attackAcc, UpgrdToString(upgr1.power), UpgrdToString(upgr2.power));
                posY += rowH;
                if (powerName1 != null && powerName1 != "")
                {
                    DrawTableCell2(g, posX, ref posY, secondPartW - nameW, powerName1, attackPower + " " + MergedUpgrdDamage(
                        UpgrdDamageToString(attack1, attack2, upgr1), UpgrdDamageToString(attack1, attack2, upgr2)));
                }
                DrawTableCell(g, posX, posY, "Ini:",
                    CalcLeveledValue(attack1.initiative, unit.level, level, unit.dyn_upg_lv, upgr1.initiative, upgr2.initiative).ToString(),
                    UpgrdToString(upgr1.initiative), UpgrdToString(upgr2.initiative));
                string targetsCount = "";
                string attackTarget = AttackTarget(attack1, ref targetsCount);
                if (attack1.alt_attack.value != null)
                {
                    Gattack altAttack1 = attack1.alt_attack.value;
                    string targetsCount2 = "";
                    string attackTarget2 = AttackTarget(altAttack1, ref targetsCount2);
                    if (attackTarget == "")
                        attackTarget = attackTarget2;
                    else
                        attackTarget += " " + TranslationHelper.Instance().Tr("or") + " " + attackTarget2;
                    if (targetsCount.Trim() != targetsCount2.Trim())
                    {
                        if (targetsCount == "")
                            targetsCount = targetsCount2;
                        else
                            targetsCount += " " + TranslationHelper.Instance().Tr("or") + " " + targetsCount2;
                    }
                }
                posY += rowH;
                DrawTableCell2(g, posX, ref posY, secondPartW - nameW, "Area:", attackTarget);
                DrawTableCell2(g, posX, ref posY, secondPartW - nameW, "Targets:", targetsCount);
                posY += (int)(rowH * 0.5);


                if (upgrBinding.ContainsKey(unit.unit_id))
                {
                    int index = 0;
                    int maxUnitW = secondPartW / upgrBinding[unit.unit_id].Count() - 5;
                    int iconW = Math.Min(maxUnitW * 3 / 4, 75);
                    int unitOffset = (maxUnitW - iconW) / 2;
                    g.DrawString(TranslationHelper.Instance().Tr("Upgrade to:"), baseFont, brush,
                                new RectangleF(posX, posY, secondPartW, rowH), sf);
                    posY += rowH;
                    foreach (string id in upgrBinding[unit.unit_id])
                    {
                        Gunit next = model.GetObjectT<Gunit>(id);
                        string nextIconId = next.base_unit.key + "FACEB";
                        if (next.base_unit.value == null)
                            nextIconId = next.unit_id + "FACEB";
                        nextIconId = nextIconId.ToUpper();
                        if (HasResource("Faces", nextIconId))
                        {
                            Image prevIm = GetImageById("Faces", nextIconId);
                            prevIm = GameResourceModel.MakeTransparent(ref prevIm);
                            int prevX = posX + index * maxUnitW;
                            Rectangle imgRect = new Rectangle(prevX + unitOffset, posY, iconW, iconW);
                            g.DrawImage(prevIm, imgRect);
                            string prevName = next.name_txt.value.text;
                            result.clickables.Add(new ClickableArea()
                            {
                                id = id,
                                pos = imgRect,
                                tooltip = prevName
                            });
                            index++;
                        }
                    }
                }
                if (unit.race_id.value.race_type.value.text == "L_NEUTRAL")
                {

                }
                else if (unit.race_id.value.race_type.value.text == "L_ELF")
                {
                    Image raceIm = GetImageById("Interf", "DLG_CHOOSE_LORD_ELF");
                    raceIm = GameResourceModel.ResizeImage(raceIm, new Size(35, 40));
                    g.DrawImage(raceIm, new Rectangle(image.Width - 85 - offsetX, offsetY, raceIm.Width, raceIm.Height));
                }
                else
                {
                    Image raceIm = GetImageById("Interf", "_MENU_RACE_" + unit.race_id.value.ShortName() + "_N");
                    raceIm = GameResourceModel.ResizeImage(raceIm, new Size(35, 40));
                    g.DrawImage(raceIm, new Rectangle(image.Width - 85 - offsetX, offsetY, raceIm.Width, raceIm.Height));
                }
                if (withHelp)
                { 
                    string header = TranslationHelper.Tr_("units_view_help");
                    Rectangle helpRect = new Rectangle(image.Width - offsetX - 35, offsetY, 25, 25);
                    g.DrawEllipse(new Pen(StyleService.GetColor("imageFont")), helpRect);

                    result.clickables.Add(new ClickableArea()
                    {
                        id = "?",
                        pos = helpRect,
                        tooltip = header,
                        recommendMaxW = 400
                    });
                    helpRect.Y += 2;
                    g.DrawString("?", new Font("comic sans", 15, FontStyle.Bold), brush, helpRect, sf);
                }
            }
            result.image = image;
            //List<string> fields = new List<string>();
            //fields.Add("prev_id");
            //fields.Add("race_id");
            //result.properties = GetProperties(unit, fields);
            result.obj = unit;
            result.desc = objectDesc;
            return result;
        }

        public static List<Gmodif> UnitModifiersList(Gunit unit)
        {
            List<Gmodif> modifs = new List<Gmodif>();
            foreach (var modList in unit.modifs.value)
            {
                if (modList.modif_1.value != null) modifs.Add(modList.modif_1.value);
                if (modList.modif_2.value != null) modifs.Add(modList.modif_2.value);
                if (modList.modif_3.value != null) modifs.Add(modList.modif_3.value);
                if (modList.modif_4.value != null) modifs.Add(modList.modif_4.value);
                if (modList.modif_5.value != null) modifs.Add(modList.modif_5.value);
                if (modList.modif_6.value != null) modifs.Add(modList.modif_6.value);
                if (modList.modif_7.value != null) modifs.Add(modList.modif_7.value);
                if (modList.modif_8.value != null) modifs.Add(modList.modif_8.value);
                if (modList.modif_9.value != null) modifs.Add(modList.modif_9.value);
            }
            return modifs;
        }

        ViewItem InfoProvider.GetView(IViewIntent intent)
        {
            inAction = true;
            ViewIntentBase baseIntent = intent as ViewIntentBase;
            string newId = baseIntent.id.ToLower();
            unitId = newId;
            Gunit unit = model.GetObjectT<Gunit>(newId);
            if (unit == null)
            {
                Logger.LogError("Failed to get unit: " + newId);
                ViewItem result = new ViewItem();
                result.image = StyleService.GetImage("back");
                return result;
            }
            if (!baseIntent.ignoreSettings)
            {
                levelUpDown.Minimum = unit.level;
                if (newId != currentId || (int)levelUpDown.Value < unit.level)
                    levelUpDown.Value = unit.level;
                currentId = newId;
                inAction = false;
                return ProcessUnit(unit, (int)levelUpDown.Value, baseIntent.showHelp, true);
            }
            else
            {
                currentId = newId;
                return ProcessUnit(unit, unit.level, baseIntent.showHelp);
            }
        }

        public List<InfoEntry> GetAllEntries()
        {
            return allItems;
        }
        IEnumerable<Control> InfoProvider.GetControls()
        {
            return new Control[] { label, levelUpDown, expLabel, expUpDown, giveExpButton };
        }

        private void GiveExp(object sender, EventArgs e)
        {
            Gunit unit = model.GetObjectT<Gunit>(unitId);
            int baseExp = СalcConsumedExp(unitId, unit.level);
            int exp = (int)expUpDown.Value - baseExp;
            int resLevel = unit.level;
            GDynUpgr upgr1 = unit.dyn_upg1.value;
            GDynUpgr upgr2 = unit.dyn_upg2.value;
            while (true)
            {
                if (resLevel >= 100)
                    break;
                int requeredExp = CalcLeveledValue(unit.xp_next, unit.level, resLevel, unit.dyn_upg_lv, upgr1.xp_next, upgr2.xp_next);
                if (requeredExp > exp)
                    break;
                resLevel++;
                exp -= requeredExp;
            }
            levelUpDown.Value = resLevel;
        }

        private void LevelChanged(object sender, EventArgs e)
        {
            if (inAction)
                return;
            _handler?.Invoke();
        }
    }
}
