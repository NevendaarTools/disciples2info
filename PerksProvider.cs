﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Disciples2Info
{
    public class PerksProvider : ViewProviderBase, InfoProvider
    {
        int offsetX = 140;
        int offsetY = 80;
        Font nameFont = new Font("comic sans", 15, FontStyle.Bold);
        Font baseFont = new Font("comic sans", 9, FontStyle.Bold);

        string InfoProvider.GetInfoType()
        {
            return "LR";
        }

        public PerksProvider(GameModel model_)
        {
            model = model_;
            foreach (Glord item in model.GetAllT< Glord>())
            {
                string race = item.race_id.value.name_txt.value.text;
                allItems.Add(new InfoEntry()
                {
                    context = race + " - " + item.desc_txt.value.text,
                    id = item.lord_id,
                    item = item
                }); 
            }
            allItems.Sort((x, y) => String.Compare(x.context, y.context));
        }

        public ViewItem GetView(IViewIntent intent)
        {
            ViewItem result = new ViewItem();
            ViewIntentBase baseIntent = intent as ViewIntentBase;
            currentId = baseIntent.id;
            bool withHelp = baseIntent.showHelp;
            Image image = StyleService.GetImage("back");
            offsetX = StyleService.GetCustomInt("offsetX");
            offsetY = StyleService.GetCustomInt("offsetY");
            if (image == null)
            {
                image = new Bitmap(520 + offsetX* 2, 520 + offsetY* 2);
                using (Graphics gfx = Graphics.FromImage(image))
                using (SolidBrush brush = new SolidBrush(StyleService.GetColor("back")))
                {
                    gfx.FillRectangle(brush, 0, 0, image.Width, image.Height);
                }
            }
            else
                image = GameResourceModel.ResizeImage(image, new Size(800, 680));

            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

            StringFormat sfPerk = new StringFormat();
            sfPerk.LineAlignment = StringAlignment.Center;
            sfPerk.Alignment = StringAlignment.Near;

            Glord lord = model.GetObjectT<Glord>(baseIntent.id);
            List<GleaUpg> perks = new List<GleaUpg>();
            foreach(GleaUpg perk in model.GetAllT<GleaUpg>())
            {
                if (perk.belongs_to.key.OrdinalEquals(lord.lord_id))
                    perks.Add(perk);
            }

            perks = perks.OrderBy(p => p.min_level).ToList();

            if (perks.Count > 42)
            {
                image = GameResourceModel.ResizeImage(image, new Size((int)(image.Width * 1.5), 680));
                offsetX += 80;
            }

            using (Graphics g = Graphics.FromImage(image))
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                int posY = offsetY;
                string fullname = lord.name_txt.value.text;
                g.DrawString(fullname, nameFont, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(offsetX + 10, posY,
                    image.Width - offsetX * 2 - 20, 30), sf);
                int extraOffset = 5;
                int rowH = 20;
                posY = offsetY + 40;
                if (withHelp)
                {
                    string header = TranslationHelper.Tr_("lords_view_help");
                    Rectangle helpRect = new Rectangle(image.Width - offsetX - 30, offsetY, 25, 25);
                    g.DrawEllipse(new Pen(StyleService.GetColor("imageFont")), helpRect);

                    result.clickables.Add(new ClickableArea()
                    {
                        id = "?",
                        pos = helpRect,
                        tooltip = header
                    });
                    helpRect.Y += 2;
                    g.DrawString("?", nameFont, new SolidBrush(StyleService.GetColor("imageFont")),
                        helpRect, sf);
                }

                for (int i = 0; i < perks.Count; ++i)
                {
                    if (posY + rowH > image.Height - offsetY)
                    {
                        extraOffset += 250;
                        posY = offsetY + 40;
                    }
                    Rectangle imgRect = new Rectangle(offsetX + extraOffset, posY, 240, rowH);
                    Color rectColor = FetchBackColor(perks[i].name_txt.value.text, Color.Empty);
                    if (rectColor != Color.Empty)
                        g.FillRectangle(new SolidBrush(rectColor), imgRect);
                    g.DrawString(perks[i].min_level.ToString() + " " + 
                        RemoveStyles(perks[i].name_txt.value.text) + " [" + perks[i].priority.ToString() + "]", 
                        baseFont, new SolidBrush(FetchColor(perks[i].name_txt.value.text, StyleService.GetColor("imageFont"))),
                            imgRect, sfPerk);
                    result.clickables.Add(new ClickableArea()
                    {
                        id = perks[i].modif_id.key,
                        pos = imgRect,//new Rectangle(offsetX + extraOffset + 4, posY, 240, rowH - 8),
                        tooltip = RemoveStyles(perks[i].desc_txt.value.text)
                    });
                    posY += rowH + 2;
                }
            }
            result.image = image;
            result.obj = lord;
            return result;
        }
        public bool FilterAcceptItem(InfoEntry entry, List<FilterValue> filters)
        {
            string filter = (string)filters[0].GetValue();
            return filter == "" || entry.context.OrdinalContains(filter) || entry.id.OrdinalContains(filter);
        }
    }
}
