using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TreeView;

namespace Disciples2Info
{
    public class EvolutionProvider : ViewProviderBase, InfoProvider
    {
        private readonly Font nameFont = new Font("comic sans", 11, FontStyle.Bold);
        private readonly Font descFont = new Font("comic sans", 9);
        private const int offsetX = 140;
        private const int offsetY = 110;
        private const int unitWidth = 90;
        private const int smallUnitSpacing = 100;
        private const int largeUnitSpacing = 160;
        private const int verticalSpacing = 140;

        private Dictionary<string, List<string>> evolutionTree = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);

        public string GetInfoType()
        {
            return "EP";
        }

        public EvolutionProvider(GameModel model_)
        {
            model = model_;
            allItems.Clear();
            Dictionary<string, int> branchCounter = new Dictionary<string, int>();

            // First pass - build evolution tree mapping
            foreach (Gunit unit in model.GetAllT<Gunit>())
            {
                if (unit.prev_id.value != null)
                {
                    string prevId = unit.prev_id.key;
                    if (!evolutionTree.ContainsKey(prevId))
                    {
                        evolutionTree[prevId] = new List<string>();
                    }
                    evolutionTree[prevId].Add(unit.unit_id);
                }
            }

            // Second pass - create entries for each evolution path
            foreach (Gunit unit in model.GetAllT<Gunit>())
            {
                // Skip units that have a previous evolution stage
                if (unit.prev_id.value != null)
                    continue;

                // Skip units that don't have any evolutions
                if (!evolutionTree.ContainsKey(unit.unit_id))
                    continue;

                string branchId = "0000EP" + unit.unit_id;
                string raceName = "";
                if (unit.race_id.value != null)
                {
                    raceName = unit.race_id.value.name_txt.value.text + " - ";
                }
                string context = raceName + unit.name_txt.value.text;
                allItems.Add(new InfoEntry()
                {
                    context = context,
                    id = branchId,
                    item = unit
                });
            }
            allItems.Sort((x, y) => String.Compare(x.context, y.context));
        }

        private void DrawUnitNode(Graphics g, Gunit unit, int x, int y, ViewItem result)
        {
            // Draw unit image
            string unitIconId = unit.base_unit.key + "FACE";
            if (unit.base_unit.key == null || unit.base_unit.key.ToUpper() == "G000000000")
                unitIconId = unit.unit_id + "FACE";
            Image unitImage = GetImageById("Faces", unitIconId.ToUpper());
            if (unitImage != null)
            {
                // Resize image and improve rendering quality
                unitImage = GameResourceModel.ResizeImage(unitImage, new Size((int)(unitImage.Width * 1.35), (int)(unitImage.Height * 1.35)));
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                Rectangle imgRect = new Rectangle(x - unitImage.Width / 2, y - unitImage.Height / 2, unitImage.Width, unitImage.Height);
                g.DrawImage(unitImage, imgRect);

                Rectangle borderRect = new Rectangle(imgRect.X - 5, imgRect.Y - 5, imgRect.Width + 10, imgRect.Height + 10);
                if (unit.size_small && HasResource("Icons", "BORDERUNITSMALL"))
                {
                    Image borderImage = GetImageById("Icons", "BORDERUNITSMALL");
                    g.DrawImage(borderImage, borderRect);
                }
                else if (!unit.size_small && HasResource("Icons", "BORDERUNITLARGE"))
                {
                    Image borderImage = GetImageById("Icons", "BORDERUNITLARGE");
                    g.DrawImage(borderImage, borderRect);
                }

                string tooltip = "";
                if (unit.upgrade_b.value != null)
                {
                    tooltip = unit.upgrade_b.value.build_id;
                }
                if (unit.enroll_b.value != null)
                {
                    tooltip = unit.enroll_b.value.build_id;
                }
                if (tooltip != "")
                {
                    Gbuild build = model.GetObjectT<Gbuild>(tooltip);
                    tooltip = TranslationHelper.Instance().Tr("Living in:")+" ";
                    tooltip += build.name_txt.value.text;
                }

                // Add clickable area for the unit
                result.clickables.Add(new ClickableArea()
                {
                    id = unit.unit_id,
                    pos = imgRect,
                    tooltip = tooltip
                });

                // Draw only unit name with centered alignment
                StringFormat sf = new StringFormat();
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;
                int nameH = 32;
                
                var back = StyleService.GetImage("info");
                if (back == null)
                    back = new Bitmap(20, 20);
                int tooltipW = 30 + unit.name_txt.value.text.Trim().Length * 10;
                Rectangle nameRect = new Rectangle(borderRect.X + borderRect.Width / 2 - tooltipW / 2, borderRect.Y - nameH, tooltipW, nameH);
                
                g.DrawImage(back, nameRect);
                g.DrawString(unit.name_txt.value.text, nameFont,
                    new SolidBrush(StyleService.GetColor("imageFont")),
                    nameRect, sf);
                // Add to description list
                result.desc.Add(unit.name_txt.value.text + " [" + unit.unit_id + "]");
            }
        }

        private int GetHorizontalSpacing(string unitId)
        {
            var unit = model.GetObjectT<Gunit>(unitId);
            return unit?.size_small == true ? smallUnitSpacing : largeUnitSpacing;
        }

        private int CalculateSubtreeWidth(string unitId, HashSet<string> visited)
        {
            if (visited.Contains(unitId) || !evolutionTree.ContainsKey(unitId))
                return unitWidth;

            visited.Add(unitId);
            var evolvedUnits = evolutionTree[unitId];
            if (evolvedUnits.Count == 0)
                return unitWidth;

            int totalWidth = 0;
            foreach (var evolvedId in evolvedUnits)
            {
                totalWidth += CalculateSubtreeWidth(evolvedId, visited);
            }

            // Use spacing based on the current unit's size
            int spacing = GetHorizontalSpacing(unitId);
            return Math.Max(unitWidth, totalWidth + (evolvedUnits.Count - 1) * spacing);
        }

        private void DrawEvolutionTree(Graphics g, string rootUnitId, int x, int y, ViewItem result, HashSet<string> drawnUnits, bool drawingLines)
        {
            if (drawnUnits.Contains(rootUnitId))
                return;

            drawnUnits.Add(rootUnitId);
            Gunit rootUnit = model.GetObjectT<Gunit>(rootUnitId);
            if (rootUnit == null)
                return;

            // Draw current unit only in second pass
            if (!drawingLines)
            {
                DrawUnitNode(g, rootUnit, x, y, result);
            }

            // Draw evolved units if any
            if (evolutionTree.ContainsKey(rootUnitId))
            {
                List<string> evolvedUnits = evolutionTree[rootUnitId];
                
                // Calculate spacing based on image size and subtree widths
                Image sampleImage = GetImageById("Faces", rootUnit.unit_id + "FACE");
                if (sampleImage != null)
                {
                    int scaledWidth = (int)(sampleImage.Width * 1.35);
                    int scaledHeight = (int)(sampleImage.Height * 1.35);
                    
                    // Calculate widths of all subtrees
                    List<int> subtreeWidths = new List<int>();
                    int totalWidth = 0;
                    foreach (var evolvedId in evolvedUnits)
                    {
                        HashSet<string> visited = new HashSet<string>();
                        int subtreeWidth = CalculateSubtreeWidth(evolvedId, visited);
                        subtreeWidths.Add(subtreeWidth);
                        totalWidth += subtreeWidth;
                    }

                    // Use spacing based on the current unit's size
                    int spacing = GetHorizontalSpacing(rootUnitId);
                    totalWidth += (evolvedUnits.Count - 1) * spacing;

                    // Center the entire evolution tree
                    int evolvedX = x - totalWidth / 2;
                    int evolvedY = y + scaledHeight + verticalSpacing;

                    // Calculate parent unit center
                    int parentCenterX = x + scaledWidth / 2;
                    int parentCenterY = y + scaledHeight / 2;

                    for (int i = 0; i < evolvedUnits.Count; i++)
                    {
                        string evolvedUnitId = evolvedUnits[i];
                        int subtreeWidth = subtreeWidths[i];
                        int centerX = evolvedX + subtreeWidth / 2;

                        // Calculate child unit center
                        int childCenterX = centerX;
                        int childCenterY = evolvedY + scaledHeight / 2;

                        // Draw connection line only in first pass
                        if (drawingLines)
                        {
                            Pen linePen = new Pen(StyleService.GetColor("imageFont"), 8);
                            g.DrawLine(linePen, parentCenterX - sampleImage.Width / 2, parentCenterY, 
                                centerX + 3, childCenterY - 85);
                        }

                        // Draw evolved unit
                        DrawEvolutionTree(g, evolvedUnitId, centerX, evolvedY, result, drawnUnits, drawingLines);
                        evolvedX += subtreeWidth + spacing;
                    }
                }
            }
        }

        private Image CropImageToContent(Image sourceImage, Color backgroundColor, ViewItem result, int padding = 20)
        {
            // Create temporary bitmap for scanning
            Bitmap bmp = new Bitmap(sourceImage);

            // Find bounds
            int left = bmp.Width;
            int top = bmp.Height;
            int right = 0;
            int bottom = 0;

            // Scan for content
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    Color pixel = bmp.GetPixel(x, y);
                    if (pixel.ToArgb() != backgroundColor.ToArgb())
                    {
                        left = Math.Min(left, x);
                        top = Math.Min(top, y);
                        right = Math.Max(right, x);
                        bottom = Math.Max(bottom, y);
                    }
                }
            }

            // Add padding
            left = Math.Max(0, left - padding);
            top = Math.Max(0, top - padding);
            right = Math.Min(bmp.Width - 1, right + padding);
            bottom = Math.Min(bmp.Height - 1, bottom + padding);

            // Create cropped image
            int croppedWidth = right - left + 1;
            int croppedHeight = bottom - top + 1;
            
            Image resultImage = sourceImage;
            if (croppedWidth > 0 && croppedHeight > 0)
            {
                Rectangle cropRect = new Rectangle(left, top, croppedWidth, croppedHeight);
                resultImage = new Bitmap(croppedWidth, croppedHeight);
                using (Graphics cropG = Graphics.FromImage(resultImage))
                {
                    cropG.DrawImage(bmp, new Rectangle(0, 0, croppedWidth, croppedHeight),
                                      cropRect, GraphicsUnit.Pixel);
                }

                // Adjust clickable areas
                foreach (var clickable in result.clickables)
                {
                    clickable.pos = new Rectangle(
                        clickable.pos.X - left,
                        clickable.pos.Y - top,
                        clickable.pos.Width,
                        clickable.pos.Height
                    );
                }
            }

            bmp.Dispose();
            return resultImage;
        }

        public ViewItem GetView(IViewIntent intent)
        {
            ViewItem result = new ViewItem();
            string itemId = (intent as ViewIntentBase)?.id;
            string unitId = itemId.Substring(6, 10);
            if (unitId == null)
                return result;

            Gunit unit = model.GetObjectT<Gunit>(unitId);
            if (unit == null)
                return result;

            string rootUnitId = unitId;
            Gunit currentUnit = unit;
            while (currentUnit?.prev_id.value != null)
            {
                rootUnitId = currentUnit.prev_id.key;
                currentUnit = model.GetObjectT<Gunit>(rootUnitId);
            }

            HashSet<string> visited = new HashSet<string>();
            int totalTreeWidth = CalculateSubtreeWidth(rootUnitId, visited);
            
            int imageWidth = totalTreeWidth + offsetX * 4; 
            int imageHeight = 1200;
            
            Image image = new Bitmap(imageWidth, imageHeight);
            using (Graphics gfx = Graphics.FromImage(image))
            using (SolidBrush brush = new SolidBrush(StyleService.GetColor("back")))
            {
                gfx.FillRectangle(brush, 0, 0, image.Width, image.Height);
            }

            Graphics g = Graphics.FromImage(image);
            g.Clear(StyleService.GetColor("back"));
            int startX = image.Width / 2 - 75;

            // First pass - draw lines
            HashSet<string> drawnUnits = new HashSet<string>();
            DrawEvolutionTree(g, rootUnitId, startX, offsetY, result, drawnUnits, true);

            // Second pass - draw units
            drawnUnits.Clear();
            DrawEvolutionTree(g, rootUnitId, startX, offsetY, result, drawnUnits, false);

            g.Dispose();

            // Crop the image to content and adjust clickable areas
            image = CropImageToContent(image, StyleService.GetColor("back"), result);

            result.image = image;
            return result;
        }
    }
}