﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Disciples2Info
{
    class StyleSelectWidget: ComboBox
    {
        SettingsManager manager;
        public StyleSelectWidget()
        {
            SelectedIndexChanged += selectedIndexChanged;
        }

        public void LoadList(SettingsManager manager_, GameModel model)
        {
            Items.Clear();
            manager = manager_;
            string styleName = manager_.Value("style", "default");
            if (!manager_.HasValue("style"))
                manager_.SetValue("style", "default");
            StyleService tmp = StyleService.Instance();
            tmp.gameData = model;
            int index = 0;
            foreach (Style style in StyleService.Instance().styles)
            {
                string name = style.name;
                Items.Add(name);
                if (name == styleName)
                    index = Items.Count - 1;
            }
            SelectedIndex = index;

        }

        private void selectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedIndex < 0)
                return;
            StyleService.Instance().SetStyle(SelectedIndex);
            manager.SetValue("style", StyleService.Instance().current.name);
        }

    }
}
