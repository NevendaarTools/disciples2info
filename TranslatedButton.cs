﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Disciples2Info
{
    public class TranslatedButton : Button
    {
        public string TranslatedText;
        public Point TextOffset;

        public TranslatedButton()
        {
            FlatStyle = FlatStyle.Flat;
        }

        public void TranslateText()
        {
            if (Tag is string text)
            {
                TranslatedText = TranslationHelper.Instance().Tr(text);
                Width = (int)Graphics.FromHwnd(Handle).MeasureString(TranslatedText, Font).Width + TextOffset.X * 2 + 1;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (String.IsNullOrEmpty(Text) && !String.IsNullOrEmpty(TranslatedText))
            {
                using (var brush = new SolidBrush(ForeColor))
                {
                    e.Graphics.DrawString(TranslatedText, Font, brush, TextOffset);
                }
            }
        }
    }
}
