@echo off

set current_dir=%CD%
set parent_dir=%CD%\..
set tools_dir=%parent_dir%\tools

if not exist %tools_dir% (
    cd %parent_dir%
    git clone https://NevendaarTools@bitbucket.org/NevendaarTools/tools.git
	cd %current_dir%
)

md bin\Release
md bin\Debug

mklink /D %CD%\bin\Release\Styles %CD%\Styles
mklink /D %CD%\bin\Debug\Styles %CD%\Styles

echo Done.
