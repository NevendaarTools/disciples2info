﻿
namespace Disciple2Info
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.parseGameButton = new System.Windows.Forms.Button();
            this.selectPathButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.aboutButton = new System.Windows.Forms.Button();
            this.pathComboBox = new System.Windows.Forms.ComboBox();
            this.logButton = new System.Windows.Forms.Button();
            this.langComboBox = new System.Windows.Forms.ComboBox();
            this.styleSelectWidget1 = new Disciples2Info.StyleSelectWidget();
            this.universalInfoView1 = new Disciples2Info.UniversalInfoView();
            this.buttonCheckUpdates = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // parseGameButton
            // 
            this.parseGameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.parseGameButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.parseGameButton.Location = new System.Drawing.Point(652, 7);
            this.parseGameButton.Name = "parseGameButton";
            this.parseGameButton.Size = new System.Drawing.Size(96, 23);
            this.parseGameButton.TabIndex = 24;
            this.parseGameButton.Text = "Parse";
            this.parseGameButton.UseVisualStyleBackColor = true;
            this.parseGameButton.Click += new System.EventHandler(this.parseGameButton_Click);
            // 
            // selectPathButton
            // 
            this.selectPathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectPathButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectPathButton.Location = new System.Drawing.Point(574, 7);
            this.selectPathButton.Name = "selectPathButton";
            this.selectPathButton.Size = new System.Drawing.Size(29, 23);
            this.selectPathButton.TabIndex = 23;
            this.selectPathButton.Text = "...";
            this.selectPathButton.UseVisualStyleBackColor = true;
            this.selectPathButton.Click += new System.EventHandler(this.selectPathButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(323, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Game path:";
            // 
            // aboutButton
            // 
            this.aboutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aboutButton.Location = new System.Drawing.Point(7, 7);
            this.aboutButton.Name = "aboutButton";
            this.aboutButton.Size = new System.Drawing.Size(95, 23);
            this.aboutButton.TabIndex = 28;
            this.aboutButton.Text = "About";
            this.aboutButton.UseVisualStyleBackColor = true;
            this.aboutButton.Click += new System.EventHandler(this.aboutButton_Click);
            // 
            // pathComboBox
            // 
            this.pathComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pathComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pathComboBox.FormattingEnabled = true;
            this.pathComboBox.Location = new System.Drawing.Point(391, 8);
            this.pathComboBox.Name = "pathComboBox";
            this.pathComboBox.Size = new System.Drawing.Size(177, 21);
            this.pathComboBox.TabIndex = 29;
            this.pathComboBox.SelectedIndexChanged += new System.EventHandler(this.pathComboBox_SelectedIndexChanged);
            // 
            // logButton
            // 
            this.logButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.logButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logButton.Location = new System.Drawing.Point(609, 7);
            this.logButton.Name = "logButton";
            this.logButton.Size = new System.Drawing.Size(37, 23);
            this.logButton.TabIndex = 30;
            this.logButton.Text = "Log";
            this.logButton.UseVisualStyleBackColor = true;
            this.logButton.Click += new System.EventHandler(this.logButton_Click);
            // 
            // langComboBox
            // 
            this.langComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.langComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.langComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.langComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.langComboBox.FormattingEnabled = true;
            this.langComboBox.Location = new System.Drawing.Point(754, 6);
            this.langComboBox.Name = "langComboBox";
            this.langComboBox.Size = new System.Drawing.Size(53, 24);
            this.langComboBox.TabIndex = 31;
            this.langComboBox.SelectedIndexChanged += new System.EventHandler(this.langComboBox_SelectedIndexChanged);
            // 
            // styleSelectWidget1
            // 
            this.styleSelectWidget1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.styleSelectWidget1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.styleSelectWidget1.FormattingEnabled = true;
            this.styleSelectWidget1.Location = new System.Drawing.Point(201, 8);
            this.styleSelectWidget1.Name = "styleSelectWidget1";
            this.styleSelectWidget1.Size = new System.Drawing.Size(116, 21);
            this.styleSelectWidget1.TabIndex = 27;
            this.styleSelectWidget1.TabStop = false;
            // 
            // universalInfoView1
            // 
            this.universalInfoView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.universalInfoView1.Location = new System.Drawing.Point(3, 36);
            this.universalInfoView1.Name = "universalInfoView1";
            this.universalInfoView1.Size = new System.Drawing.Size(813, 484);
            this.universalInfoView1.TabIndex = 0;
            // 
            // buttonCheckUpdates
            // 
            this.buttonCheckUpdates.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCheckUpdates.Location = new System.Drawing.Point(109, 7);
            this.buttonCheckUpdates.Name = "buttonCheckUpdates";
            this.buttonCheckUpdates.Size = new System.Drawing.Size(86, 23);
            this.buttonCheckUpdates.TabIndex = 32;
            this.buttonCheckUpdates.Text = "Updates";
            this.buttonCheckUpdates.UseVisualStyleBackColor = true;
            this.buttonCheckUpdates.Click += new System.EventHandler(this.buttonCheckUpdates_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 512);
            this.Controls.Add(this.buttonCheckUpdates);
            this.Controls.Add(this.langComboBox);
            this.Controls.Add(this.logButton);
            this.Controls.Add(this.pathComboBox);
            this.Controls.Add(this.aboutButton);
            this.Controls.Add(this.styleSelectWidget1);
            this.Controls.Add(this.universalInfoView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.parseGameButton);
            this.Controls.Add(this.selectPathButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(830, 550);
            this.Name = "Form1";
            this.Text = "Disciples2Info";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button parseGameButton;
        private System.Windows.Forms.Button selectPathButton;
        private System.Windows.Forms.Label label2;
        private Disciples2Info.StyleSelectWidget styleSelectWidget1;
        private System.Windows.Forms.Button aboutButton;
        private Disciples2Info.UniversalInfoView universalInfoView1;
        private System.Windows.Forms.ComboBox pathComboBox;
        private System.Windows.Forms.Button logButton;
        private System.Windows.Forms.ComboBox langComboBox;
        private System.Windows.Forms.Button buttonCheckUpdates;
    }
}

