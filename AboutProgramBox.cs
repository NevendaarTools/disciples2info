﻿using NevendaarTools;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace Disciples2Info
{
    partial class AboutProgramBox : Form
    {
        public AboutProgramBox()
        {
            InitializeComponent();
            this.Text = String.Format(TranslationHelper.Tr_("About {0}"), AssemblyTitle);
            this.labelProductName.Text = AssemblyProduct;
            this.labelVersion.Text = String.Format(TranslationHelper.Tr_("Version {0}"), AssemblyVersion);
            this.textBoxDescription.Text = AssemblyDescription;
            this.sourceLabel.Text = TranslationHelper.Tr_("Source Code:");
            this.sourceLinkLabel.Text = "https://bitbucket.org/NevendaarTools/disciples2info/";
            if (TranslationHelper.Instance().lang() == "ru")
                FillDescRu();
            else
                FillDesc();
            StyleService.ApplyStyle(this);
            this.Refresh();
            this.Invalidate();
        }

        void FillDesc()
        {
            this.textBoxDescription.Text += "Author: Vilgeforc( https://discord.com/users/Vilgeforc#9911 )";
            this.textBoxDescription.Text += "\nIn case of any questions, suggestions or bugs, you can contact me directly or ask in the discord server: https://discord.gg/uZRjA8yvUH ";
            this.textBoxDescription.Text += "\n\nSpecial thanks:";
            this.textBoxDescription.Text += "\n- Тимур Findme (his discord server has united many people who are now working on improving the game)\n";
            this.textBoxDescription.Text += "    https://www.twitch.tv/findmetv \n    https://www.youtube.com/channel/UCLoJJqPGvuOV1UNRpWoIGgQ";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- Mak and Unven (they fixed many bugs of the original game and made many fantastic improvements)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- NorvezskayaSemga( https://norvezskayasemga.pro/ ), \n  Motlin(author of dark style images https://dis2modding.fandom.com/ru/wiki/Мод_Мотлина ) \n  Nazar( https://disk.yandex.ru/d/99GTrRNNX4dX9w )\n";
            this.textBoxDescription.Text += " (the authors of wonderful modifications for the game, many times they told me what and where to find in the game files)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- KtodDos (beta tester of the first versions, author of the text guide to the program)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- RockWolf (author of the English translation, added the ability for the program to auto-update, admin of the \"Libraries of Nevendaar\" https://discord.gg/h5sMubqz4X)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- Positiff_Pff (Added a filters)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- Dedication (Added ward and immunity filter)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- NorvezskayaSemga (Added mapmaker help tab)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- as well as many other wonderful people who prolong the life of our favorite game by creating mods, sagas, maps and new incredible tracks";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "All the people whose names are listed above directly helped in the development and without them it would be very difficult to create this program.\n";

        }

        void FillDescRu()
        {
            this.textBoxDescription.Text += "Автор: Vilgeforc( https://discord.com/users/Vilgeforc#9911 )";
            this.textBoxDescription.Text += "\nВ случае любых вопросов, предложений или багов вы можете написать мне напрямую либо задать вопрос в дискорде: https://discord.gg/uZRjA8yvUH ";
            this.textBoxDescription.Text += "\n\nБлагодарности:";
            this.textBoxDescription.Text += "\n- Тимур Findme (его дискорд сервер объединил многих людей, трудящихся над улучшением игры)\n";
            this.textBoxDescription.Text += "    https://www.twitch.tv/findmetv \n    https://www.youtube.com/channel/UCLoJJqPGvuOV1UNRpWoIGgQ";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- Mak and Unven (исправили множество багов игры и привнесли в нее множество фантастических улучшений)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- NorvezskayaSemga( https://norvezskayasemga.pro/ ), \n  Motlin( автор изображений для темной темы https://dis2modding.fandom.com/ru/wiki/Мод_Мотлина ) \n  Nazar( https://disk.yandex.ru/d/99GTrRNNX4dX9w )\n";
            this.textBoxDescription.Text += " (авторы замечательных модификаций к игре, множество раз подсказывали мне где и что найти в файлах игры)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- KtodDos (бета тестер ранних версий программы, автор документа гайда по ней)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- RockWolf (автор перевода на аглийский язык, добавил возможность программе автообновляться, админ \"Библиотек Невендаара\" https://discord.gg/h5sMubqz4X)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- Positiff_Pff (Добавил фильтры)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- Dedication (Добавил фильтры защит и иммунитетов)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- NorvezskayaSemga (Добавил вкладку для картоделов)\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "- А так же множеству других замечательных людей, продлевающих жизнь нашей любимой игре, создавая моды, саги, карты и невероятное музыкальное сопровождение";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "\n";
            this.textBoxDescription.Text += "Все люди, чьи имена указаны выше, непосредственно помогали мне в процессе разработки и без них было очень сложно создать эту программу.\n";
        }

        #region Методы доступа к атрибутам сборки

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void sourceLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(sourceLinkLabel.Text);
        }

        private void textBoxDescription_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }
    }
}
