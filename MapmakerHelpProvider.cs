﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Disciples2Info
{
    class MapmakerHelpProvider : ViewProviderBase, InfoProvider
    {
        List<string> specialCats = UnitsProvider.SpecialCats();

        private string currentIntentId = null;

        Font nameFont = new Font("comic sans", 15, FontStyle.Bold);
        Font descFont = new Font("comic sans", 15);
        int offsetX = 180;
        int rowH = 36;

        TranslatedButton giveExpButton = new TranslatedButton();

        Dictionary<string, MapmakerHelpItem> items = new Dictionary<string, MapmakerHelpItem>();

        class MapmakerHelpItem
        {
            public string key;
            public int filterIndex;

            public string id;
            public VType valueType;
            public VType additionalValueType;

            public Tglobal valText;
            public Gmodif valModif;
            public GItem valItem;
            public Gunit valUnit;
            public Image valImage;
            public List<Gmodif> valModifList;

            public enum VType
            {
                Text,
                Modif,
                Image,
                GItem,
                GUnit,
                ModifList
            }

            public MapmakerHelpItem(string _key, string _id, Tglobal val)
            {
                key = _key;
                filterIndex = 2;
                id = _id;

                valueType = VType.Text;
                valText = val;
            }
            public MapmakerHelpItem(string _key, string _id, Gmodif val)
            {
                key = _key;
                filterIndex = 0;
                id = _id;

                valueType = VType.Modif;
                valModif = val;
            }
            public MapmakerHelpItem(string _key, string _id, Image val)
            {
                key = _key;
                filterIndex = 1;
                id = _id;

                valueType = VType.Image;
                valImage = val;
            }
            public MapmakerHelpItem(string _key, string _id, GItem val)
            {
                key = _key;
                filterIndex = 3;
                id = _id;

                valueType = VType.GItem;
                valItem = val;
            }
            public MapmakerHelpItem(string _key, string _id, Gunit val)
            {
                key = _key;
                filterIndex = 3;
                id = _id;

                valueType = VType.GUnit;
                valUnit = val;
            }

            public MapmakerHelpItem(string _key, string _id)
            {
                key = _key;
                id = _id;
            }
            public void SetModifListData(Gunit val)
            {
                filterIndex = 4;

                valueType = VType.ModifList;
                additionalValueType = VType.GUnit;

                valUnit = val;
                valModifList = UnitsProvider.UnitModifiersList(val);
            }
            public void SetModifListData(GItem val)
            {
                filterIndex = 4;

                valueType = VType.ModifList;
                additionalValueType = VType.GItem;

                valItem = val;
                valModifList = new List<Gmodif>();
                if (!(val.mod_potion.value is null))
                {
                    valModifList.Add(val.mod_potion.value);
                }
                if (!(val.mod_equip.value is null))
                {
                    valModifList.Add(val.mod_equip.value);
                }
            }

        }

        class ColorMap
        {
            public Size Size;
            public Color[,] map;

            public ColorMap(Bitmap img)
            {
                if (img == null) return;

                Size = new Size(img.Size.Width, img.Size.Height);
                map = new Color[img.Size.Width, img.Size.Height];

                for (int y = 0; y < img.Size.Height; y++)
                {
                    for (int x = 0; x < img.Size.Width; x++)
                    {
                        map[x, y] = img.GetPixel(x, y);
                    }
                }
            }

            public static bool AreEqual(ColorMap map1, ColorMap map2)
            {
                if ((map1 == null) != (map2 == null)) return false;
                if (map1.Size != map2.Size) return false;

                for (int y = 0; y < map1.Size.Height; y++)
                {
                    for (int x = 0; x < map1.Size.Width; x++)
                    {
                        if (!AreEqual(map1.map[x, y], map2.map[x, y]))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            public static bool AreEqual(Color c1, Color c2)
            {
                if (c1.R != c2.R) return false;
                if (c1.G != c2.G) return false;
                if (c1.B != c2.B) return false;
                if (c1.A != c2.A) return false;
                return true;
            }
        }

        public string GetInfoType()
        {
            return "MH";
        }

        public MapmakerHelpProvider(GameModel model_)
        {
            items.Clear();

            giveExpButton.Tag = "To Clipboard";
            giveExpButton.Height = new NumericUpDown().Height;
            giveExpButton.TextOffset = new Point(8, 3);
            giveExpButton.Click += TXTToClipboard;
            giveExpButton.FlatStyle = FlatStyle.Flat;
            giveExpButton.TranslateText();

            model = model_;

            ComboBoxFilter typeFilter = new ComboBoxFilter();
            typeFilter.Init(model, new Size(107, 20), "<Category>",
                (entry, str) => ((MapmakerHelpItem)entry.item).filterIndex == (int)str);

            typeFilter.AddVariant(new FilterValue(0, "Scen editor modifiers"));
            typeFilter.AddVariant(new FilterValue(4, "Units and items modifiers"));
            typeFilter.AddVariant(new FilterValue(1, "Modifiers icons"));
            typeFilter.AddVariant(new FilterValue(2, "TGlobal strings"));
            typeFilter.AddVariant(new FilterValue(3, "Attack reach"));
            filters.Add(typeFilter);

            foreach (Gmodif modif in model.GetAllT<Gmodif>())
            {
                string id = modif.modif_id;
                if (id.Substring(6,1) =="9" )
                {
                    string key = GetModifierDesc(modif);
                    key = RemoveStyles(key);
                    items.Add(key + '+' + id, new MapmakerHelpItem(key, id, modif));
                    allItems.Add(new InfoEntry()
                    {
                        context = key,
                        id = "0000MH" + id,
                        item = items[key + '+' + id]
                    });
                }
            }

            GameResource icons = model._ResourceModel.GetResource("Icons");
            List<string> skipIcon = new List<string>(new string[] {"ILLUSION", "INVISIBLE", "ICNSY001" });
            for (int i = 0; i < 25; i++)
            {
                if (i > 90)
                    skipIcon.Add("ABIL0" + i.ToString());
                if (i > 9)
                    skipIcon.Add("ABIL00" + i.ToString());
                else
                    skipIcon.Add("ABIL000" + i.ToString());
            }
            foreach (string t in new string[] { "ITEM", "LORDLARGE", "LORDSMALL", "UNITLARGE", "UNITSMALL" })
            {
                skipIcon.Add("BORDER" + t);
            }
            foreach (string t in new string[] { "", "DW", "EL", "HE", "HU", "UN" })
            {
                if (t != "")
                {
                    skipIcon.Add("CITY" + t);
                    if (t != "UN")
                        skipIcon.Add("LOGO" + t);
                    else
                        skipIcon.Add("LOGO" + "UD");
                }
                for (int i = 1; i < 6; i++)
                {
                    skipIcon.Add("CITY" + t + i.ToString());
                }
            }

            List<ColorMap> addedIcons = new List<ColorMap>();
            foreach (string icon_id in icons.GetAllIndexOptKeys())
            {
                string id = icon_id;
                if (!(skipIcon.Contains(id)))
                {
                    string key = "ICON_" + id;
                    List<Image> imgs = icons.GetFramesById_(icon_id);
                    if (imgs.Count > 0)
                    {
                        ColorMap m = new ColorMap(imgs[0] as Bitmap);
                        bool add = true;
                        foreach (ColorMap img in addedIcons)
                        {
                            if (ColorMap.AreEqual(img, m))
                            {
                                add = false;
                                break;
                            }
                        }
                        if (add)
                        {
                            items.Add(key + '+' + id, new MapmakerHelpItem(key, id, imgs[0]));
                            allItems.Add(new InfoEntry()
                            {
                                context = key,
                                id = "0000MH" + id,
                                item = items[key + '+' + id]
                            });
                            addedIcons.Add(m);
                        }
                    }
                }
            }
;
            foreach (Tglobal t in model.GetAllT<Tglobal>())
            {
                string id = t.txt_id;
                string key = RemoveStyles(t.text);
                if (items.ContainsKey(key) == false)
                {
                    items.Add(key + '+' + id, new MapmakerHelpItem(key, id, t));
                    allItems.Add(new InfoEntry()
                    {
                        context = key,
                        id = "0000MH" + id,
                        item = items[key + '+' + id]
                    });
                }
            }

            foreach (GItem item in model.GetAllT<GItem>())
            {
                string id = item.item_id;
                string key = RemoveStyles(item.name_txt.value.text);
                items.Add(key + '+' + id, new MapmakerHelpItem(key, id, item));
                allItems.Add(new InfoEntry()
                {
                    context = key,
                    id = "0000MH" + id,
                    item = items[key + '+' + id]
                });
            }

            foreach (Gunit unit in model.GetAllT<Gunit>())
            {
                string id = unit.unit_id;
                string key = RemoveStyles(unit.name_txt.value.text);

                if (specialCats.Contains(unit.unit_cat.value.text, StringComparer.OrdinalIgnoreCase))
                    key += " [" + TranslationHelper.Instance().Tr(unit.unit_cat.value.text) + "]";
                items.Add(key + '+' + id, new MapmakerHelpItem(key, id, unit));
                allItems.Add(new InfoEntry()
                {
                    context = key,
                    id = "0000MH" + id,
                    item = items[key + '+' + id]
                });
            }

            foreach (GItem item in model.GetAllT<GItem>())
            {
                string id = item.item_id;
                string key = RemoveStyles(item.name_txt.value.text);
                MapmakerHelpItem m = new MapmakerHelpItem(key, id);
                m.SetModifListData(item);
                items.Add(key + '=' + id, m);
                allItems.Add(new InfoEntry()
                {
                    context = key,
                    id = "0001MH" + id,
                    item = items[key + '=' + id]
                });
            }

            foreach (Gunit unit in model.GetAllT<Gunit>())
            {
                string id = unit.unit_id;
                string key = RemoveStyles(unit.name_txt.value.text);

                if (specialCats.Contains(unit.unit_cat.value.text, StringComparer.OrdinalIgnoreCase))
                    key += " [" + TranslationHelper.Instance().Tr(unit.unit_cat.value.text) + "]";
                MapmakerHelpItem m = new MapmakerHelpItem(key, id);
                m.SetModifListData(unit);
                items.Add(key + '=' + id, m);
                allItems.Add(new InfoEntry()
                {
                    context = key,
                    id = "0001MH" + id,
                    item = items[key + '=' + id]
                });
            }

            allItems.Sort((x, y) => String.Compare(x.context, y.context));
        }
        public ViewItem GetView(IViewIntent intent)
        {
            currentIntentId = null;
            ViewItem result = new ViewItem();
            ViewIntentBase vIntent = intent as ViewIntentBase;
            MapmakerHelpItem item = null;
            foreach (InfoEntry iEntry in allItems)
            {
                if (iEntry.id == vIntent.id)
                {
                    item = iEntry.item as MapmakerHelpItem;
                    break;
                }
            }
            if (item is null)
                return result;

            currentIntentId = vIntent.id;

            Image image = StyleService.GetImage("back");
            if (image == null)
            {
                image = new Bitmap(1000, 600);
                using (Graphics gfx = Graphics.FromImage(image))
                using (SolidBrush brush = new SolidBrush(StyleService.GetColor("back")))
                {
                    gfx.FillRectangle(brush, 0, 0, image.Width, image.Height);
                }
            }
            else
                image = GameResourceModel.ResizeImage(image, new Size(1000, 600));

            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

            Image itemIm;
            string title;
            string desc;
            if (item.valueType == MapmakerHelpItem.VType.Text)
            {
                itemIm = new Bitmap(10, 10);

                title = item.valText.txt_id;
                desc = item.valText.text;
            }
            else if(item.valueType == MapmakerHelpItem.VType.Modif)
            {
                itemIm = GetImage(item.valModif);
                itemIm = GameResourceModel.ResizeImage(itemIm, new Size(itemIm.Width * 2, itemIm.Height * 2));

                title = item.valModif.modif_id;
                desc = GetModifierDesc(item.valModif);
            }
            else if(item.valueType == MapmakerHelpItem.VType.Image)
            {
                itemIm = item.valImage;
                itemIm = GameResourceModel.ResizeImage(itemIm, new Size(itemIm.Width * 2, itemIm.Height * 2));

                title = item.id;
                desc = "";
            }
            else if(item.valueType == MapmakerHelpItem.VType.GUnit)
            {
                itemIm = GetImage(item.valUnit);
                itemIm = GameResourceModel.ResizeImage(itemIm, new Size((int)(itemIm.Width * 1.35), (int)(itemIm.Height * 1.35)));

                title = item.valUnit.name_txt.value.text;
                desc = TranslationHelper.Instance().Tr("Attack reach number") + ": " + item.valUnit.attack_id.value.reach.key;
            }
            else if(item.valueType == MapmakerHelpItem.VType.GItem)
            {
                itemIm = GetImage(item.valItem);
                itemIm = GameResourceModel.ResizeImage(itemIm, new Size(itemIm.Width * 2, itemIm.Height * 2));

                title = item.valItem.name_txt.value.text;
                if (item.valItem.attack_id.value is null)
                {
                    desc = TranslationHelper.Instance().Tr("Don't have an attack");
                }
                else
                {
                    desc = TranslationHelper.Instance().Tr("Attack reach number") + ": " + item.valItem.attack_id.value.reach.key;
                }
            }
            else if (item.valueType == MapmakerHelpItem.VType.ModifList)
            {
                if (item.additionalValueType == MapmakerHelpItem.VType.GUnit)
                {
                    itemIm = GetImage(item.valUnit);
                    itemIm = GameResourceModel.ResizeImage(itemIm, new Size((int)(itemIm.Width * 1.35), (int)(itemIm.Height * 1.35)));

                    title = item.valUnit.name_txt.value.text;
                }
                else if (item.additionalValueType == MapmakerHelpItem.VType.GItem)
                {
                    itemIm = GetImage(item.valItem);
                    itemIm = GameResourceModel.ResizeImage(itemIm, new Size(itemIm.Width * 2, itemIm.Height * 2));

                    title = item.valItem.name_txt.value.text;
                }
                else
                {
                    itemIm = new Bitmap(10, 10);
                    title = "";
                }
                if (item.valModifList.Count > 0)
                {
                    if (item.additionalValueType == MapmakerHelpItem.VType.GItem)
                    {
                        desc = item.valItem.desc_txt.value.text + Environment.NewLine;
                    }
                    else
                    {
                        desc = "";
                    }
                    foreach (Gmodif modif in item.valModifList)
                    {
                        if (desc != "")
                            desc += Environment.NewLine;
                        desc += modif.modif_id + " (" + GetModifierDesc(modif) + ")";
                    }
                }
                else
                {
                    desc = TranslationHelper.Instance().Tr("Don't have modifiers");
                }
            }
            else
            {
                itemIm = new Bitmap(10, 10);
                title = "";
                desc = "";
            }

            var textBrush = new SolidBrush(StyleService.GetColor("imageFont"));
            using (Graphics g = Graphics.FromImage(image))
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                int posY = 80;
                int secondPartW = image.Width - offsetX * 2 - itemIm.Width - 10;

                Color rectColor = FetchBackColor(title, Color.Empty);
                if (rectColor != Color.Empty)
                    g.FillRectangle(new SolidBrush(rectColor), new RectangleF(offsetX + 10 + itemIm.Width, posY, secondPartW, 30));
                
                g.DrawString(RemoveStyles(title), nameFont,
                    new SolidBrush(FetchColor(title, StyleService.GetColor("imageFont"))),
                    //textBrush,
                    new RectangleF(offsetX + 10 + itemIm.Width, posY,
                    secondPartW, 30), sf);
                g.DrawString(RemoveStyles(desc.Replace("\\n", " ")), descFont, textBrush,
                    new RectangleF(offsetX + 10 + itemIm.Width, posY + rowH * 2, secondPartW, 360));
                //g.DrawRectangle(new Pen(Color.Red), new Rectangle(offsetX, posY + unitIm.Height + 34, firstPartW, 86));
                g.DrawImage(itemIm, new Rectangle(offsetX, posY, itemIm.Width, itemIm.Height));
            }

            result.image = image;
            return result;
        }

        private Image GetImage(Gunit unit)
        {
            string unitIconId = unit.base_unit.key + "FACE";
            if (unit.base_unit.value == null)
                unitIconId = unit.unit_id + "FACE";
            return GetImageById("Faces", unitIconId.ToUpper());
        }
        private Image GetImage(GItem item)
        {
            if (HasResource("IconItem", item.item_id.ToUpper()))
                return GetImageById("IconItem", item.item_id.ToUpper());
            else if (item.item_cat == 8)
                return GetImageById("IconItem", "SCROLLHU");
            else
                return new Bitmap(10, 10);
        }
        private Image GetImage(Gmodif modif)
        {
            if (HasResource("Icons", modif.modif_id.ToUpper()))
                return GetImageById("Icons", modif.modif_id.ToUpper());
            else
                return new Bitmap(10, 10);
        }


        private string GetModifierDesc(Gmodif modif)
        {
            if (modif.desc_txt.value is null)
            {
                if (modif.effects.value.Count > 0)
                    return modif.effects.value[0].desc.value.text;
                else
                    return TranslationHelper.Instance().Tr("No effect description");
            }
            else
            {
                return modif.desc_txt.value.text;
            }
        }
        
        public bool FilterAcceptItem(InfoEntry entry, List<FilterValue> filters)
        {
            string filter = (string)filters[0].GetValue();
            return filter == "" || entry.context.OrdinalContains(filter) || entry.id.OrdinalEquals(filter);
        }

        IEnumerable<Control> InfoProvider.GetControls()
        {
            return new Control[] { giveExpButton };
        }

        private void TXTToClipboard(object sender, EventArgs e)
        {
            MapmakerHelpItem item = null;
            foreach (InfoEntry iEntry in allItems)
            {
                if (iEntry.id == currentIntentId)
                {
                    item = iEntry.item as MapmakerHelpItem;
                    break;
                }
            }
            if (item is null)
                return;

            string result = "";
            if (item.valueType == MapmakerHelpItem.VType.Text)
            {
                result = "Id.new('" + item.valText.txt_id + "')";
            }
            else if (item.valueType == MapmakerHelpItem.VType.Modif)
            {
                result = "Id.new('" + item.valModif.modif_id + "')";
            }
            else if (item.valueType == MapmakerHelpItem.VType.Image)
            {
                result = "'" + item.id.ToUpper() + "'";
            }
            else if (item.valueType == MapmakerHelpItem.VType.GUnit)
            {
                result = item.valUnit.attack_id.value.reach.key.ToString();
            }
            else if (item.valueType == MapmakerHelpItem.VType.GItem)
            {
                if (!(item.valItem.attack_id.value is null))
                {
                    result = item.valItem.attack_id.value.reach.key.ToString();
                }
            }
            else if (item.valueType == MapmakerHelpItem.VType.ModifList)
            {
                if (item.valModifList.Count > 0)
                {
                    foreach (Gmodif modif in item.valModifList)
                    {
                        if (result != "")
                            result += Environment.NewLine;
                        result += "Id.new('" + modif.modif_id + "')";
                    }
                }
            }

            if (result != "")
                Clipboard.SetText(result);
        }
    }
}
