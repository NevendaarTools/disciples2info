rmdir /S /Q build
mkdir build
cd build 
mkdir release
mkdir debug
cd ..

MSBuild.exe Disciple2Info.sln /t:Clean,Build /p:Configuration=Release
MSBuild.exe Disciple2Info.sln /t:Clean,Build /p:Configuration=Debug

xcopy bin\Release\Disciples2Info.exe build\release /R /I /Y /S
xcopy bin\Release\*.tr build\release /R /I /Y /S
xcopy bin\Debug\Disciples2Info.exe build\debug /R /I /Y /S
xcopy bin\Debug\*.tr build\debug /R /I /Y /S

xcopy Updater\*.exe build\debug /R /I /Y /S
xcopy Updater\*.exe build\release /R /I /Y /S

xcopy /e/s/i "Styles" "build/Debug/Styles" 
xcopy /e/s/i "Styles" "build/Release/Styles"

cd build/release
"C:\Program Files\7-Zip\7z.exe" a -tzip -mx5 -r0 ../Disciples2Info_release.zip
cd..
cd debug
"C:\Program Files\7-Zip\7z.exe" a -tzip -mx5 -r0 ../Disciples2Info_debug.zip
cd ../..