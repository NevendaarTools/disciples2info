﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
//using System.Timers;

namespace Disciples2Info
{
    public class InfoEntry
    {
        public string context;
        public string id;
        public Object item;
    }
    public class ClickableArea
    {
        enum ViewMode
        {
            Any = 0,
            Edit
        };
        public Rectangle pos;
        public string id;
        public string tooltip;
        public int recommendMaxW = 240;
        public int recommendMinW = 100;
        ViewMode mode = ViewMode.Any;
    }

    public class ViewItem
    {
        public Image image;
        public List<ClickableArea> clickables = new List<ClickableArea>();
        public Object obj = null;
        public List<string> desc = new List<string>();
    }

    public interface IViewIntent
    {
        string GetProviderType();
    }

    public class ViewIntentBase: IViewIntent
    {
        public ViewIntentBase() { }
        public ViewIntentBase(string id_)
        {
            id = id_;
        }
        public string id;
        public string GetProviderType()
        {
            if (id == null || id == "null" || id.Length < 6)
                return "";
            string type = id.Substring(4, 2).ToUpper();
            return type;
        }
        public bool ignoreSettings = false;
        public bool showHelp = true;
    }

    public class CopyIntent : ViewIntentBase
    {
        
    }

    public class UnitViewIntent : ViewIntentBase
    {
        public int level = 0;
    }

    public class FilterValue 
    {
        private object _value;
        private string _label;
        private string _text;

        public FilterValue(object value)
        {
            _value = value;
        }
        public FilterValue(object value, string label)
        {
            _value = value;
            _label = label;
            Translate();
        }

        public void Translate() => _text = TranslationHelper.Instance().Tr(_label);
        public object GetValue() => _value;
        public override string ToString() => _text ?? _value.ToString();
    }

    public delegate void UpdateStatusEventHandler();
    public interface InfoProvider
    {
        string GetInfoType();
        ViewItem GetView(IViewIntent intent);
        IViewIntent GetCurrentIntent();
        List<InfoEntry> GetAllEntries();
        IEnumerable<Control> GetControls();
        bool FilterAcceptItem(InfoEntry entry, List<ItemFilter> filters);
        void ConnectUpdateSlot(UpdateStatusEventHandler handler);
        void RegisterFilters();
        List<ItemFilter> GetItemFilters();
        event EventHandler FilterChanged;
    }

    public delegate bool AcceptDelegate(InfoEntry entry, object value);


    public interface ItemFilter
    {
        void Init(GameModel model, Size size, string name, AcceptDelegate getter);
        bool AcceptItem(InfoEntry entry);
        bool HasActiveFilter();
        Control GetControl();
        void ResetFilter();
        event EventHandler Changed;
    }

    public class StringFilter : ItemFilter
    {
        public event EventHandler Changed;
        TextBox text;
        string filter = "";
        private Timer debounceTimer = new Timer(); 


        public bool AcceptItem(InfoEntry entry)
        {
            return (filter == "" || entry.context.OrdinalContains(filter) || entry.id.OrdinalContains(filter));
        }

        public Control GetControl()
        {
            return text;
        }

        public void Setup(TextBox textBox)
        {
            debounceTimer.Interval = 400;
            debounceTimer.Tick -= OnDebounceTimerElapsed;
            debounceTimer.Tick += OnDebounceTimerElapsed;
            text = textBox;
            text.TextChanged -= FilterValueChanged;
            text.TextChanged += FilterValueChanged;
        }

        public void Init(GameModel model, Size size, string name, AcceptDelegate getter)
        {
            
        }

        public void ResetFilter()
        {
            text.Text = "";
        }
        private void FilterValueChanged(object sender, EventArgs e)
        {
            if (filter == text.Text)
                return;
            filter = text.Text;
            debounceTimer.Stop();
            debounceTimer.Start();
            //Changed(sender, e);
        }
        private void OnDebounceTimerElapsed(object sender, EventArgs e)
        {
            Changed(sender, e);
            debounceTimer.Stop();
        }

        public bool HasActiveFilter()
        {
            return filter != "";
        }
    }

    public class LimitFilter : ItemFilter
    {
        public event EventHandler Changed;
        NumericUpDown textMin;
        NumericUpDown textMax;
        Label textName;
        FlowLayoutPanel holder;
        public GetValueDelegate _getDelegate;
        public delegate int GetValueDelegate(InfoEntry entry);


        public bool AcceptItem(InfoEntry entry)
        {
            int value = _getDelegate(entry);
            if (textMin.Value != 0 && textMin.Value > value)
                return false;
            if (textMax.Value != 0 && textMax.Value < value)
                return false;
            return true;
        }

        public Control GetControl()
        {
            return holder;
        }

        public void Init(GameModel model, Size size, string name, AcceptDelegate getter)
        {
            holder = new FlowLayoutPanel();
            holder.Width = size.Width;
            holder.Height = size.Height;
            holder.Margin = new Padding(0);
            holder.FlowDirection = FlowDirection.TopDown;
            holder.BorderStyle = BorderStyle.FixedSingle;

            textName = new Label();
            textName.TextAlign = ContentAlignment.MiddleCenter;
            textName.Text = name;
            textName.Tag = name;
            textName.Width = size.Width;
            textName.Height = 20;

            textMin = new NumericUpDown();
            textMin.Minimum = 0;
            textMin.Maximum = 9999;
            textMin.Width = size.Width - 10;
            textMin.Height = 20;
            textMin.TextAlign = HorizontalAlignment.Center;

            textMax = new NumericUpDown();
            textMax.Minimum = 0;
            textMax.Maximum = 9999;
            textMax.Width = size.Width - 10;
            textMax.Height = 20;
            textMax.TextAlign = HorizontalAlignment.Center;

            holder.Controls.Add(textName);
            holder.Controls.Add(textMin);
            holder.Controls.Add(textMax);

            textMin.TextChanged += FilterValueChanged;
            textMax.TextChanged += FilterValueChanged;
        }

        public void ResetFilter()
        {
            textMin.Value = 0;
            textMax.Value = 0;
        }
        private void FilterValueChanged(object sender, EventArgs e)
        {
            Changed(sender, e);
        }

        public bool HasActiveFilter()
        {
            return textMin.Value == 0 && textMax.Value == 0;
        }
    }

    public class ComboBoxFilter: ItemFilter
    {
        public void Init(GameModel model, Size size, string name, AcceptDelegate getter)
        {
            combo = new ComboBox() { Size = new Size(93, 20), FlatStyle = FlatStyle.Flat };
            combo.Items.Add(new FilterValue(null, name));
            combo.SelectedIndex = 0;
            combo.SelectedIndexChanged += FilterValueChanged;
            _getter = getter;
        }

        public void AddVariant(FilterValue val)
        {
            combo.Items.Add(val);
        }
        public bool AcceptItem(InfoEntry entry)
        {
            if (combo.SelectedIndex == 0)
                return true;
            FilterValue filter = (FilterValue)combo.SelectedItem;
            var value = filter.GetValue();
            if (value == null)
                return true;
            return _getter(entry, value);
        }
        public Control GetControl()
        {
            return combo;
        }
        public void ResetFilter()
        {
            combo.SelectedIndex = 0;
        }
        ComboBox combo;
        public event EventHandler Changed;
        AcceptDelegate _getter;

        private void FilterValueChanged(object sender, EventArgs e)
        {
            if (Changed != null)
                Changed(sender, e);
        }

        public bool HasActiveFilter()
        {
            return combo.SelectedIndex != 0;
        }
    }

    public class InfoManager
    {
        Dictionary<string, InfoProvider> providers = new Dictionary<string, InfoProvider>();
        InfoProvider current = null;
        IViewIntent currentIntent = null;

        public void Clear()
        {
            current = null;
            currentIntent = null;
        }
        public void UnregisterAll()
        {
            providers.Clear();
        }
        public void RegisterProvider(InfoProvider provider)
        {
            providers.Add(provider.GetInfoType(), provider);
        }
        public List<InfoEntry> GetAllEntries(string id)
        {
            string type = id.Substring(4, 2).ToUpper();
            return GetAllEntriesByType(type);
        }
        public List<InfoEntry> GetAllEntriesByType(string type)
        {
            if (providers.ContainsKey(type))
                return providers[type].GetAllEntries();
            return new List<InfoEntry>();
        }

        public string GetCurrentType()
        {
            if (current == null)
                return null;
            return current.GetInfoType();
        }

        public IViewIntent GetCurrentIntent()
        {
            return currentIntent;
        }

        public IEnumerable<Control> GetControls()
        {
            return providers.Values.SelectMany(provider => provider.GetControls());
        }
        public IEnumerable<Control> GetControls(string type)
        {
            return type != null && providers.ContainsKey(type) ? providers[type].GetControls() : new Control[0];
        }

        public List<ItemFilter> GetItemFilters(string type)
        {
            return type != null && providers.ContainsKey(type) ? providers[type].GetItemFilters() : new List<ItemFilter>();
        }

        public bool HasProvider(string id)
        {
            return providers.ContainsKey(id.ToUpper());
        }

        public ViewItem GetView(IViewIntent intent)
        {
            currentIntent = intent;
            string type = intent.GetProviderType();
            if (providers.ContainsKey(type))
            {
                current = providers[type];
                return current.GetView(intent);
            }
            return new ViewItem();
        }

        public void SelectCurrent(string type)
        {
            if (HasProvider(type))
                current = providers[type];
        }

        public bool FilterAcceptItem(InfoEntry entry, List<ItemFilter> filters)
        {
            string type = entry.id.Substring(4, 2).ToUpper();
            if (providers.ContainsKey(type))
                return providers[type].FilterAcceptItem(entry, filters);
            return true;
        }
    }

    public class ImageHelper
    {
        public static int DrawCostValues(Graphics g, int x, int y, Image resource, int val, int rowH, Font font)
        {
            if (val > 0)
            {
                g.DrawImage(resource, new Rectangle(x, y, rowH, rowH));
                g.DrawString(val.ToString(), font, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(x + 40, y,
                    80, rowH));
                return 120;
            }
            return 0;
        }

        public static void DrawTableCell(Graphics g, int x, int y, string name, string value, 
            int rowH, int rowW, Font titleFont, Font baseFont, int rowCount = 1)
        {
            int w = rowW;
            if (name == "")
                w = 0;
            g.DrawString(TranslationHelper.Instance().Tr(name), titleFont, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(x, y, rowW, rowH * rowCount));
            g.DrawString(value, baseFont, new SolidBrush(StyleService.GetColor("imageFont")),
                new RectangleF(x + w, y, 170, rowH * rowCount));
        }

        public static void DrawTableCell(Graphics g, int x, int y, string name, string value1, 
            string value2, string value3, int rowH, int rowW, Font titleFont, Font font, int rowCount = 1)
        {
            int w = rowW;
            if (name == "")
                w = 0;
            g.DrawString(TranslationHelper.Instance().Tr(name), titleFont, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(x, y, 170, rowH * rowCount));
            string extra = "";
            if (value2 == "0" && value3 == "0")
                extra = "";
            else
            {
                if (value2 == value3)
                    extra = "(" + value2 + ")";
                else
                {
                    extra = "(" + value2 + " / " + value3 + ")";
                }
            }
            g.DrawString(value1, font, new SolidBrush(StyleService.GetColor("imageFont")),
                new RectangleF(x + w, y, 170, rowH * rowCount));
            float value1Width = g.MeasureString(value1, font).Width;
            g.DrawString(extra, font, new SolidBrush(StyleService.GetColor("imageFont")),
                new RectangleF(x + w + value1Width, y, 170, rowH * rowCount));
        }


        public static void DrawTableCell(Graphics g, int x, int y, string name, string value1, string value2,
            int rowH, int rowW, Font font)
        {
            int w = rowW;
            if (name == "")
                w = 0;
            g.DrawString(TranslationHelper.Instance().Tr(name), font, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(x, y, 140, 30));
            string extra = "";
            if (value2 != "")
                extra = "(" + value2 + ")";
            g.DrawString(value1, font, new SolidBrush(StyleService.GetColor("imageFont")),
                new RectangleF(x + w, y, 170, rowH));
            float value1Width = g.MeasureString(value1, font).Width;
            g.DrawString(extra, font, new SolidBrush(StyleService.GetColor("imageFont")),
                new RectangleF(x + w + value1Width, y, 170, rowH));
        }
    }

    public class ViewProviderBase : InfoProvider
    {
        protected GameModel model;
        protected List<InfoEntry> allItems = new List<InfoEntry>();
        protected string currentId;
        protected UpdateStatusEventHandler _handler;
        protected List<string> transfCats = new List<string>() { "L_TRANSFORM_OTHER", "L_SUMMON", "L_TRANSFORM_SELF" };
        protected TextBox filterText = new TextBox() { Size = new Size(189, 20), BorderStyle = BorderStyle.FixedSingle };
        protected List<ItemFilter> filters = new List<ItemFilter>();
        protected List<string> objectDesc = new List<string>();
        public event EventHandler FilterChanged;
        public void RegisterFilters()
        {
            for (int i = 0; i < filters.Count; i++)
            {
                var filter = filters[i];
                filter.Changed -= onFilter;
                filter.Changed += onFilter;
            }
        }

        public void onFilter(object sender, EventArgs e)
        {
            if (FilterChanged != null)
                FilterChanged(sender, e);
        }
        public static string RemoveStyles(string baseText)
        {
            string text = baseText;
            text = text.Replace("\\t", "");
            text = text.Replace("\\n", "\n");
            while (true)
            {
                bool found = false;
                int index = text.IndexOf("\\c");
                if (index != -1)
                {
                    found = true;
                    text = text.Remove(index, 14);
                }
                index = text.IndexOf("\\o");
                if (index != -1)
                {
                    found = true;
                    text = text.Remove(index, 14);
                }

                if (!found)
                    break;
            }
            while (true)
            {
                int index = text.IndexOf('\\');
                if (index == -1)
                    break;
                int endIndex = text.IndexOf(';', index);
                if (endIndex == -1)
                    endIndex = text.Length - 1;

                text = text.Remove(index, endIndex - index + 1);
            }
            return text;
        }
        public static Color FetchColor(string text, Color defaultColor)
        {
            int index = text.IndexOf("\\c");
            if (index != -1)
            {
                string colorText = text.Substring(index + 2, 12);
                var colorArray = colorText.Split(';');
                return Color.FromArgb(255, int.Parse(colorArray[0]),
                    int.Parse(colorArray[1]),
                    int.Parse(colorArray[2]));
            }
            return defaultColor;
        }
        public static Color FetchBackColor(string text, Color defaultColor)
        {
            int index = text.IndexOf("\\o");
            if (index != -1)
            {
                string colorText = text.Substring(index + 2, 12);
                var colorArray = colorText.Split(';');
                return Color.FromArgb(255, int.Parse(colorArray[0]),
                    int.Parse(colorArray[1]),
                    int.Parse(colorArray[2]));
            }
            return defaultColor;
        }
        protected string AttackTarget(Gattack skill, ref string targetsCount)
        {
            string result;
            targetsCount = "1";
           
            if (skill.reach.key == 1)
            {
                targetsCount = "6";
                return TranslationHelper.Instance().Tr("All targets");
            }
            if (skill.reach.key == 2)
            {
                if (skill.clasS.value.text.IsIn("L_TRANSFORM_SELF")) // "L_DOPPELGANGER")
                {
                    SettingsManager settings = SettingsManager.Instance();
                    if (!settings.HasValue("hide_unit_self_reach"))
                    {
                        settings.SetValue("hide_unit_self_reach", "True");
                    }
                    if (settings.Value("hide_unit_self_reach") == "True")
                    {
                        targetsCount = "";
                        return "";
                    }
                    return TranslationHelper.Instance().Tr("Unit self");
                }
                return TranslationHelper.Instance().Tr("Any 1 target");
            }
            if (skill.reach.key == 3)
            {
                return TranslationHelper.Instance().Tr("Melee target");
            }
            if (skill.reach.value.target_txt.value != null)
            {
                targetsCount = skill.reach.value.target_txt.value.text;
                result = skill.reach.value.reach_txt.value.text;
            }
            else
                result = skill.reach.key.ToString() + " " + skill.reach.value.text;

            return result;
        }

        public List<ItemFilter> GetItemFilters()
        {
            return filters;
        }

        public int DrawCostCellsH(Graphics g, int x, int y, string name, string cost, StringFormat sf, int nameW, int rowH, Font font, int scale = 1)
        {
            int G1 = CostHelper.G(cost) * scale;
            int R1 = CostHelper.R(cost) * scale;
            int Y1 = CostHelper.Y(cost) * scale;
            int E1 = CostHelper.E(cost) * scale;
            int W1 = CostHelper.W(cost) * scale;
            int B1 = CostHelper.B(cost) * scale;

            g.DrawString(name, font, new SolidBrush(StyleService.GetColor("imageFont")),
                    new RectangleF(x, y,
                    nameW, rowH), sf);
            int valuesCount = 0;
            if (G1 > 0)
                valuesCount++;
            if (R1 > 0)
                valuesCount++;
            if (Y1 > 0)
                valuesCount++;
            if (E1 > 0)
                valuesCount++;
            if (W1 > 0)
                valuesCount++;
            if (B1 > 0)
                valuesCount++;
            int newX = x + (5 - valuesCount) * 60 + 30;
            int newY = y + rowH;

            newX += ImageHelper.DrawCostValues(g, newX, newY, GetImageById("IntfScen", "_RESOURCES_GOLD"), G1, rowH, font);
            newX += ImageHelper.DrawCostValues(g, newX, newY, GetImageById("IntfScen", "_RESOURCES_REDM_B"), R1, rowH, font);
            newX += ImageHelper.DrawCostValues(g, newX, newY, GetImageById("IntfScen", "_RESOURCES_BLUEM_B"), Y1, rowH, font);
            newX += ImageHelper.DrawCostValues(g, newX, newY, GetImageById("IntfScen", "_RESOURCES_BLACKM_B"), E1, rowH, font);
            newX += ImageHelper.DrawCostValues(g, newX, newY, GetImageById("IntfScen", "_RESOURCES_WHITEM_B"), W1, rowH, font);
            newX += ImageHelper.DrawCostValues(g, newX, newY, GetImageById("IntfScen", "_RESOURCES_GREENM_B"), B1, rowH, font);
            objectDesc.Add(name + " " + cost);
            return newY;
        }
        protected void DrawSkillTransf(Graphics g, Gattack skill, int posX, int areaW, int totalW, ref int posY, ref ViewItem result, 
            int offsetX, int limit = 4)
        {
            if (skill == null)
                return;
            if (transfCats.Contains(skill.clasS.value.text))
            {
                int index = 0;
                int prevX = posX;
                if (skill.transf.Count() < limit)
                {
                    prevX += areaW / 2 - (skill.transf.Count() * 30);
                }
                foreach (Gtransf transf_id in skill.transf.value)
                {
                    Gunit transf = transf_id.transf_id.value;
                    string transfIconId = transf.base_unit.key + "FACE";
                    if (transf.base_unit.value == null)
                        transfIconId = transf.unit_id + "FACE";
                    transfIconId = transfIconId.ToUpper();
                    if (HasResource("Faces", transfIconId))
                    {
                        Image transfIm = GetImageById("Faces", transfIconId);
                        //transfIm = MapTileHelper.ResizeImage(transfIm, new Size(40,40);
                        int imageW = 40;
                        if (!transf.size_small)
                            imageW *= 2;

                        if (prevX + imageW > totalW - offsetX)
                        {
                            prevX = posX;
                            posY += 45;
                        }
                        Rectangle imgRect = new Rectangle(prevX, posY, imageW, 40);
                        g.DrawImage(transfIm, imgRect);

                        result.clickables.Add(new ClickableArea()
                        {
                            id = transf.unit_id,
                            pos = imgRect,
                            tooltip = transf.name_txt.value.text
                        });
                        objectDesc.Add("    " + transf.name_txt.value.text + "[" + transf.unit_id + "]");
                        prevX += imageW + 5;
                        index++;
                    }
                }
                if (skill.transf.Count() > 0)
                    posY += 50;
            }
        }
        void InfoProvider.ConnectUpdateSlot(UpdateStatusEventHandler handler)
        {
            _handler = handler;
        }
        public Image GetImageById(string resourceName, string id, bool makeTransparent = true)
        {
            return model._ResourceModel.GetImageById(resourceName, id, makeTransparent);
        }
        public bool HasResource(string id)
        {
            return model._ResourceModel.HasResource(id);
        }

        public bool HasResource(string resourceName, string id)
        {
            return model._ResourceModel.HasResource(resourceName, id);
        }

        List<InfoEntry> InfoProvider.GetAllEntries()
        {
            return allItems ;
        }

        public IEnumerable<Control> GetControls()
        {
            return new Control[0];
        }

        IViewIntent InfoProvider.GetCurrentIntent()
        {
            ViewIntentBase result = new ViewIntentBase();
            result.id = currentId;
            return result;
        }

        string InfoProvider.GetInfoType()
        {
            throw new NotImplementedException();
        }

        ViewItem InfoProvider.GetView(IViewIntent intent)
        {
            throw new NotImplementedException();
        }

        public bool FilterAcceptItem(InfoEntry entry, List<ItemFilter> filters)
        {
            foreach (ItemFilter filter in filters)
                if (!filter.AcceptItem(entry))
                    return false;
            return true;
        }
    }
}
